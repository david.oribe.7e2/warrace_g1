﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class CanvasCampaign : MonoBehaviour
{
    public AudioSource audio;
    GameManager gameManager;
    [Header("Image Settings ---------------------")]
    public Sprite[] arrayCoches = new Sprite[4];
    public int carShown;
    public Image cochesImage;
    [Header("Buttons Settings ---------------------")]
    public Button btnLeftCars;
    public Button btnRightCars;
    public Button goBackBtn;
    public Button optionsBtn;
    public GameObject optionsCanvas;
    public Button startRaceBtn;
    // Start is called before the first frame update
    void Start()
    {
        carShown = 0;
        startRaceBtn.onClick.AddListener(startRace);
        optionsCanvas.SetActive(false);
        btnLeftCars.onClick.AddListener(showCarLeft);
        btnRightCars.onClick.AddListener(showCarRight);
        goBackBtn.onClick.AddListener(goBack);
        optionsBtn.onClick.AddListener(showOptions);
    }

    // Update is called once per frame
    void Update()
    {
        cochesImage.GetComponent<Image>().sprite = arrayCoches[carShown];
    }
    private void selectCar()
    {
        switch (carShown)
        {
            case 0:
                gameManager.car1 = true;
                break;
            case 1:
                gameManager.car2 = true;
                break;
            case 2:
                gameManager.car3 = true;
                break;
            case 3:
                gameManager.car4 = true;
                break;
            default:
                gameManager.car1 = true;
                break;

        }

    }
    private void startRace()
    {
        audio.Play();

        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
        selectCar();
        if (gameManager.modeSelected == GameManager.ModeSelected.GrandPrix)
        {
            SceneManager.LoadScene("CircuitoDesierto", LoadSceneMode.Single);
        } 
        else if (gameManager.modeSelected == GameManager.ModeSelected.Versus) 
        {
            SceneManager.LoadScene("VersusMode", LoadSceneMode.Single);
        }
    }
    private void goBack()
    {
        audio.Play();

        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
        gameManager.modeSelected = GameManager.ModeSelected.None;
        this.gameObject.SetActive(false);
    }
    private void showOptions()
    {
        audio.Play();

        optionsCanvas.SetActive(true);
    }
    private void showCarRight()
    {
        audio.Play();
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));

        if (carShown < arrayCoches.Length - 1)
        {
            carShown++;

            if (gameManager.modeSelected == GameManager.ModeSelected.Versus)
            {
                if (carShown==2)
                {
                    carShown++;

                }
            }
        }
    }
    private void showCarLeft()
    {
        audio.Play();
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));

        if (carShown > 0)
        {
            carShown--;
            if (gameManager.modeSelected == GameManager.ModeSelected.Versus)
            {
                if (carShown == 2)
                {
                    carShown--;

                }
            }
        }
    }
}
