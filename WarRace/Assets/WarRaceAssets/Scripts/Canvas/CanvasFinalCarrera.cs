﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class CanvasFinalCarrera : MonoBehaviour
{
    VersusModeController versusMode;
    MetaController metaController;
    GameManager gameManager;

    public int moneyTo1;
    public int moneyTo2;
    public int moneyTo3;
    public int moneyTo4;

    public List<Text> textPositionsList = new List<Text>();

    public Text textMoneyTo1;
    public Text textMoneyTo2;
    public Text textMoneyTo3;
    public Text textMoneyTo4;

    public Image ImageMoney2;
    public Image ImageMoney3;
    public Image ImageMoney4;

    public Button continueBtn;

    // Start is called before the first frame update
    void Start()
    {

        continueBtn.onClick.AddListener(continueNext);
        metaController = (MetaController)FindObjectOfType(typeof(MetaController));
        versusMode = (VersusModeController)FindObjectOfType(typeof(VersusModeController));
        //----------------------------------------
        textMoneyTo1.text = moneyTo1 + " $";
        textMoneyTo2.text = moneyTo2 + " $";
        textMoneyTo3.text = moneyTo3 + " $";
        textMoneyTo4.text = moneyTo4 + " $";
    }
    void Update()
    {
            if (metaController != null)
        {
            displayResults();
            metaController.SendMessage("lookForDeadCars");
            //metaController.SendMessage("sortListPosicionFinal");
        }
        if (versusMode != null)
        {
            displayResultsVersusMode();
            versusMode.SendMessage("lookForDeadCars");

        }
    }
    private void displayResultsVersusMode()
    {
        for (int i = 0; i < versusMode.cars.Length; i++)
        {

            textPositionsList[i].text = versusMode.cars[i];


        }
        ImageMoney2.enabled = false;
        ImageMoney3.enabled = false;
        ImageMoney4.enabled = false;
    }
    private void continueNext() //OnClick
    {
        if (metaController != null)
        {
            if (metaController.carsFinished >= 3)
            {
                gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
                // ver que modo de juego esa jugando para ver donde cambia de escena (siguiente circuito o Inicio).
                //SceneManager.LoadScene("Inicio", LoadSceneMode.Single); // de momento, para poder salir
                giveMoneyToPlayer();
                if (gameManager.modeSelected == GameManager.ModeSelected.GrandPrix)
                {
                    /*
                    for (int i=0; i<metaController.cars.Length;i++)
                    {
                        gameManager.classificationList[i]=metaController.cars[i];

                    }
                    */
                    givePoinsInGrandPrix();
                    //gameManager.classificationList = metaController.posicionFinal; HABRÁ QUE CAMBIARLO YA QUE HEMOS CAMBIADO LAS LISTAS POR ARRAYS
                }
                Time.timeScale = 1;
                gameManager.SendMessage("raceEnded");
            }
        }
        else if (versusMode != null)
        {
                    textPositionsList[0].text = versusMode.cars[0];
                    gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
            // ver que modo de juego esa jugando para ver donde cambia de escena (siguiente circuito o Inicio).
            //SceneManager.LoadScene("Inicio", LoadSceneMode.Single); // de momento, para poder salir
            PlayerPrefs.SetInt("totalMoneyEarned", PlayerPrefs.GetInt("totalMoneyEarned") + 100);
            gameManager.money += 100;
                    Time.timeScale = 1;
                    gameManager.SendMessage("raceEnded");
                
            
        }
        /*
        if (metaController.posicionFinal.Count >= 4)
        {
            gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
            // ver que modo de juego esa jugando para ver donde cambia de escena (siguiente circuito o Inicio).
            //SceneManager.LoadScene("Inicio", LoadSceneMode.Single); // de momento, para poder salir
            giveMoneyToPlayer();
            if (gameManager.modeSelected == GameManager.ModeSelected.GrandPrix)
            {
                givePoinsInGrandPrix();
                gameManager.classificationList = metaController.posicionFinal;
            }
            Time.timeScale = 1;
            gameManager.SendMessage("raceEnded");
        }
        */

    }
    /*
    private void displayResults()// array allCars
    {
        metaController.SendMessage("sortCarPositions");
        for (int i = 0; i <= metaController.allCars.Length - 1; i++)
        {
            Vehiculo coche = metaController.allCars[i].GetComponent<Vehiculo>();
            textPositionsList[i].text = coche.name + " | " + coche.tiempoTotal;
        }
    }
    */
    private void displayResults() //List<Vehiculos> posicionFinal
    {
        for(int i = 0; i < metaController.cars.Length; i++)
        {
            
                textPositionsList[i].text = metaController.cars[i];

            
        }
        /*
        if (metaController.isDeathList.Count > 0)
        {
            for (int i = 0; i < metaController.isDeathList.Count; i++)
            {
                Vehiculo coche = metaController.isDeathList[i].GetComponent<Vehiculo>();
                if (coche.isDead)
                {
                    textPositionsList[i].text = coche.name + " | DEAD";
                }
                else
                {
                    textPositionsList[i].text = coche.name + " | " + System.Math.Round(coche.tiempoTotal, 3) + " | " + coche.puntos;
                }
            }

        }
        */

    }
    private void giveMoneyToPlayer()
    {
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
        for (int i = 0; i <= 3; i++)
        {
           
                Vehiculo coche = metaController.finishedCarsArray[i].GetComponent<Vehiculo>();
                Vehiculo cocheVM = metaController.finishedCarsArray[i].GetComponent<Vehiculo>();


            if (coche.id == 0||cocheVM.id==0)
                {
                    switch (i)
                    {
                        case 0:
                            UnityEngine.Debug.Log("Player recieved " + moneyTo1);
                        PlayerPrefs.SetInt("totalMoneyEarned", PlayerPrefs.GetInt("totalMoneyEarned") + moneyTo1);
                            gameManager.money += moneyTo1;
                            break;
                        case 1:
                            UnityEngine.Debug.Log("Player recieved " + moneyTo2);
                        PlayerPrefs.SetInt("totalMoneyEarned", PlayerPrefs.GetInt("totalMoneyEarned") + moneyTo2);
                        gameManager.money += moneyTo2;
                            break;
                        case 2:
                            UnityEngine.Debug.Log("Player recieved " + moneyTo3);
                        PlayerPrefs.SetInt("totalMoneyEarned", PlayerPrefs.GetInt("totalMoneyEarned") + moneyTo3);
                        gameManager.money += moneyTo3;
                            break;
                        case 3:
                            UnityEngine.Debug.Log("Player recieved " + moneyTo4);
                        PlayerPrefs.SetInt("totalMoneyEarned", PlayerPrefs.GetInt("totalMoneyEarned") + moneyTo4);
                        gameManager.money += moneyTo4;

                            break;
                        default:
                            // de momento nada
                            break;
                    }
                }
            
        }
        /*
        for (int i = 0; i < metaController.isDeathList.Count; i++)
        {
            Vehiculo coche = metaController.isDeathList[i].GetComponent<Vehiculo>();
            if (coche.id == 0)
            {
                switch (i)
                {
                    case 0:
                        UnityEngine.Debug.Log("Player recieved " + moneyTo1);
                        gameManager.money += moneyTo1;
                        break;
                    case 1:
                        UnityEngine.Debug.Log("Player recieved " + moneyTo2);
                        gameManager.money += moneyTo2;
                        break;
                    case 2:
                        UnityEngine.Debug.Log("Player recieved " + moneyTo3);
                        gameManager.money += moneyTo3;
                        break;
                    case 3:
                        UnityEngine.Debug.Log("Player recieved " + moneyTo4);
                        gameManager.money += moneyTo4;
                        break;
                    default:
                        // de momento nada
                        break;
                }
            }
        }
        */

        
    }
    private void givePoinsInGrandPrix()
    {
        for (int i = 0; i <= 3; i++)
        {
                Vehiculo coche = metaController.finishedCarsArray[i].GetComponent<Vehiculo>();
            switch (i)
            {
                case 0:
                    coche.puntos += moneyTo1;
                    break;
                case 1:
                    coche.puntos += moneyTo2;
                    break;
                case 2:
                    coche.puntos += moneyTo3;
                    break;
                case 3:
                    coche.puntos += moneyTo4;
                    break;
                default:
                    break;
            }
        }
    }
}

