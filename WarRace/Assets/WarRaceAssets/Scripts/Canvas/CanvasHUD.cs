﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasHUD : MonoBehaviour
{
    PlayerController player;
    MetaController meta;
    public Text noVuelta;
    public Text tiempoVuelta;
    public Text tiempoMejor;

    public Image lifeBar;
    // Start is called before the first frame update
    void Start()
    {
        
    }


    void Update()
    {
        player = Object.FindObjectOfType<PlayerController>();
        meta = Object.FindObjectOfType<MetaController>();
        noVuelta.text = player.vehiculo.vueltaActual + " | " + meta.maxVueltas + " LAP";
        tiempoVuelta.text = System.Math.Round(player.vehiculo.tiempoVuelta, 3) + "";
        tiempoMejor.text = System.Math.Round(player.vehiculo.tiempoMejorVuelta, 3) + "";
        //float fillDeVida = player.vehiculo.life / player.vehiculo.maxLife;

        if(player.vehiculo.maxLife==100)
        {
            lifeBar.fillAmount = player.vehiculo.life * 0.01f;

        }else if (player.vehiculo.maxLife == 200)
        {
            lifeBar.fillAmount = player.vehiculo.life * 0.005f;

        }
        else if (player.vehiculo.maxLife == 300)
        {
            lifeBar.fillAmount = player.vehiculo.life * 0.0033f;

        }
        //Debug.Log(player.vehiculo.life );
        //Debug.Log(player.vehiculo.maxLife);
        //Debug.Log(fillDeVida);


    }
}
