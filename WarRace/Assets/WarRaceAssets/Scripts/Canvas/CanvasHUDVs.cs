﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CanvasHUDVs : MonoBehaviour
{
    PlayerController player;
    public Text score;

    public Image lifeBar;
    // Start is called before the first frame update
    void Start()
    {

    }


    void Update()
    {
        player = Object.FindObjectOfType<PlayerController>();
        //Debug.Log(player.vehiculo.life * 0.01f);

        //meta = Object.FindObjectOfType<MetaController>();
        //score.text = player.vehiculo.puntos + "";
        //float fillDeVida = player.vehiculo.life / player.vehiculo.maxLife;
        if (player!=null) {
            if (player.vehiculo.maxLife == 100)
            {
                lifeBar.fillAmount = player.vehiculo.life * 0.01f;
            }else if(player.vehiculo.maxLife == 200)
            {
                lifeBar.fillAmount = player.vehiculo.life * 0.02f;

            }
            else if (player.vehiculo.maxLife == 300)
            {
                lifeBar.fillAmount = player.vehiculo.life * 0.03f;

            }
        }
        //Debug.Log(player.vehiculo.life );
        //Debug.Log(player.vehiculo.maxLife);
        //Debug.Log(fillDeVida);


    }
}
