﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasInicio : MonoBehaviour
{
    public AudioSource audio;
    public Button startBtn;
    public Button exitBtn;
    public GameObject selectModeCanvas;
    // Start is called before the first frame update
    void Start()
    {
        selectModeCanvas.SetActive(false);
        startBtn.onClick.AddListener(goSelectModeCanvas);
        exitBtn.onClick.AddListener(exitApp);
    }
    private void goSelectModeCanvas()
    {
        audio.Play();

        selectModeCanvas.SetActive(true);
    }
    private void exitApp()
    {
        audio.Play();

        Application.Quit();
    }
}
