﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasOneRace : MonoBehaviour
{
    GameManager gameManager;
    public AudioSource audio;
    [Header("Image Settings ---------------------")]
    public Sprite[] arrayCoches = new Sprite[4];
    public int carShown;
    public Sprite[] arrayCircuitos = new Sprite[3];
    public int circuitShown;
    public Image cochesImage;
    public Image circuitosImage;
    [Header("Buttons Settings ---------------------")]
    public Button btnLeftCars;
    public Button btnRightCars;
    public Button btnLeftCircuit;
    public Button btnRightCircuit;
    public Button goBackBtn;
    public Button optionsBtn;
    public GameObject optionsCanvas;
    public Button startRaceBtn;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
        //--------------------------------------
        carShown = 0;
        circuitShown = 0;
        //-------------------------------
        startRaceBtn.onClick.AddListener(startRace);
        optionsCanvas.SetActive(false);
        btnLeftCars.onClick.AddListener(showCarLeft);
        btnRightCars.onClick.AddListener(showCarRight);
        btnLeftCircuit.onClick.AddListener(showLeftCircuit);
        btnRightCircuit.onClick.AddListener(showRightCircuit);
        goBackBtn.onClick.AddListener(goBack);
        optionsBtn.onClick.AddListener(showOptions);

    }
    void Update()
    {
        cochesImage.GetComponent<Image>().sprite = arrayCoches[carShown];
        circuitosImage.GetComponent<Image>().sprite = arrayCircuitos[circuitShown];

    }
    private void selectCircuit() {
        
        //----------------------------- (CAMBIAR NOMBRE DE LAS ESCENAS) -------------------
        switch (circuitShown)
        {
            case 0:
                SceneManager.LoadScene("CircuitoDesierto", LoadSceneMode.Single);
                break;
            case 1:
                SceneManager.LoadScene("CircuitoCarreras", LoadSceneMode.Single);
                break;                
            case 2:
                SceneManager.LoadScene("JorgeScene", LoadSceneMode.Single);
                break;                
            default:
                SceneManager.LoadScene("CircuitoDesierto", LoadSceneMode.Single);
                break;

        }
        
    }
    private void selectCar()
    {
        switch (carShown)
        {
            case 0:
                gameManager.car1 = true;
                break;
            case 1:
                gameManager.car2 = true;
                break;
            case 2:
                gameManager.car3 = true;
                break;
            case 3:
                gameManager.car4 = true;
                break;
            default:
                gameManager.car1 = true;
                break;              

        }
        
    }
    private void startRace()
    {
        //SceneManager.LoadScene("AlbaretoScene", LoadSceneMode.Single);
        // hacer que suene algo
        gameManager.modeSelected = GameManager.ModeSelected.OneRace;
        selectCar();
        selectCircuit();
    }
    private void goBack()
    {
        audio.Play();

        this.gameObject.SetActive(false);

    }
    private void showOptions()
    {
        audio.Play();

        optionsCanvas.SetActive(true);
    }
    private void showRightCircuit()
    {
        audio.Play();

        if (circuitShown < arrayCircuitos.Length - 1)
        {
            circuitShown++;
        }
    }
    private void showLeftCircuit()
    {
        audio.Play();

        if (circuitShown > 0)
        {
            circuitShown--;
        }
    }
    private void showCarRight()
    {
        audio.Play();

        if (carShown < arrayCoches.Length - 1)
        {
            carShown++;
        }
    }
    private void showCarLeft()
    {
        audio.Play();

        if (carShown > 0)
        {
            Debug.Log("left Car");
            carShown--;
        }
    }
}
