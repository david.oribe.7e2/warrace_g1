﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasOptions : MonoBehaviour
{
    public Button goBackBtn;
    public AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        goBackBtn.onClick.AddListener(goBack);
    }
    private void goBack()
    {
        audio.Play();

        this.gameObject.SetActive(false);
    }
}
