﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasPause : MonoBehaviour
{
    GameManager gameManager;
    public Button goBackBtn;
    public Button exit;
    // Start is called before the first frame update
    void Start()
    {
        goBackBtn.onClick.AddListener(goBack);
        exit.onClick.AddListener(goExit);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 1;
            this.gameObject.SetActive(false);
        }
    }
    private void goBack()
    {
        Time.timeScale = 1;
        this.gameObject.SetActive(false);
    }
    private void goExit()
    {
        Time.timeScale = 1;

        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
        gameManager.SendMessage("youDead");

    }
}
