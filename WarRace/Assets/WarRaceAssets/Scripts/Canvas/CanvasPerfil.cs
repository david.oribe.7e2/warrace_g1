﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasPerfil : MonoBehaviour
{
    public GameObject canvasOptions;
    public Button optionsBtn;
    public Button goBackBtn;

    public Image imageSurvive1;
    public Image imageSurvive10;
    public Image imageEarn100;
    public Image imageEarn1000;

    public Text percentRacesSurvived;
    public Text totalMoneyEarned;
    // Start is called before the first frame update
    void Start()
    {
        optionsBtn.onClick.AddListener(goOptions);
        goBackBtn.onClick.AddListener(goBack);
        displayAchievements();
    }
    private void displayAchievements()
    {
        if (PlayerPrefs.GetInt("racesSurvived") >= 1)
        {
            imageSurvive1.GetComponent<Image>().color = Color.yellow;
        }
        if (PlayerPrefs.GetInt("racesSurvived") >= 10)
        {
            imageSurvive10.GetComponent<Image>().color = Color.yellow;
        }
        if (PlayerPrefs.GetInt("totalMoneyEarned") >= 100)
        {
            imageEarn100.GetComponent<Image>().color = Color.yellow;

        }
        if (PlayerPrefs.GetInt("totalMoneyEarned") >= 1000)
        {
            imageEarn1000.GetComponent<Image>().color = Color.yellow;

        }
        if (PlayerPrefs.GetInt("totalRacesDone") > 0)
        {
            float percentRaces = (PlayerPrefs.GetInt("racesSurvived") * 100) / PlayerPrefs.GetInt("totalRacesDone");
            percentRacesSurvived.text = percentRaces + " %";
        }
        else
        {
            percentRacesSurvived.text = "0 %";
        }
        
        totalMoneyEarned.text = PlayerPrefs.GetInt("totalMoneyEarned") + " $";
    }
    private void goOptions()
    {
        canvasOptions.SetActive(true);
    }
    private void goBack()
    {
        this.gameObject.SetActive(false);
    }
}
