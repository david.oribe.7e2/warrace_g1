﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasSeleccionarModo : MonoBehaviour
{
    public new AudioSource audio;
    public Button oneRaceBtn;
    public GameObject oneRaceCanvas;
    public Button campaignBtn;
    public GameObject campaingCanvas;
    public Button shopBtn;
    public GameObject shopCanvas;
    public Button optionsBtn;
    public GameObject optionsCanvas;
    public Button goBackBtn;
    public Button versusBtn;
    public Button porfileBtn;
    public GameObject canvasPorfile;
    // Start is called before the first frame update
    void Start()
    {
        oneRaceBtn.onClick.AddListener(goOneRace);
        campaignBtn.onClick.AddListener(goCampaign);
        shopBtn.onClick.AddListener(goShop);
        optionsBtn.onClick.AddListener(goOptions);
        goBackBtn.onClick.AddListener(goBack);
        versusBtn.onClick.AddListener(goVersus);
        porfileBtn.onClick.AddListener(goPorfile);

        oneRaceCanvas.SetActive(false);
        campaingCanvas.SetActive(false);
        shopCanvas.SetActive(false);
        optionsCanvas.SetActive(false);
        canvasPorfile.SetActive(false);
    }
    private void goPorfile()
    {
        audio.Play();
        canvasPorfile.SetActive(true);
    }
    private void goVersus()
    {
        audio.Play();

        GameManager gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
        gameManager.modeSelected = GameManager.ModeSelected.Versus;
        campaingCanvas.SetActive(true);
    }
    private void goOneRace()
    {
        audio.Play();

        // ver si pasa a un menú y elies carrera o eliges coche, o empieza la carrera directamente
        oneRaceCanvas.SetActive(true);
    }
    private void goCampaign()
    {
        audio.Play();

        GameManager gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
        gameManager.modeSelected = GameManager.ModeSelected.GrandPrix;
        campaingCanvas.SetActive(true);
    }
    private void goShop()
    {
        audio.Play();

        shopCanvas.SetActive(true);
    }
    private void goOptions()
    {
        audio.Play();

        optionsCanvas.SetActive(true);
    }
    private void goBack()
    {
        audio.Play();

        this.gameObject.SetActive(false);
    }
    

}
