﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasShop : MonoBehaviour
{
    GameManager gameManager;
    [Header("Player Stuff ---------------------")]
    public bool haveCar1 = true;
    public bool haveCar2 = false;
    public bool haveCar3 = false;
    public bool haveCar4 = false;
    [Header("Shop Stuff ---------------------")]
    public Text moneyText;
    [Header("Car1 Stuff ----------------------")]
    public Button armorCar1Btn;
    public Text armorCar1Text;
    public Button weaponsCar1Btn;
    public Text weaponsCar1Text;
    public Button ccCar1Btn;
    public Text ccCar1Text;

    [Header("Car2 Stuff ----------------------")]
    public Button armorCar2Btn;
    public Text armorCar2Text;
    public Button weaponsCar2Btn;
    public Text weaponsCar2Text;
    public Button ccCar2Btn;
    public Text ccCar2Text;

    [Header("Car3 Stuff ----------------------")]
    public Button armorCar3Btn;
    public Text armorCar3Text;
    public Button weaponsCar3Btn;
    public Text weaponsCar3Text;
    public Button ccCar3Btn;
    public Text ccCar3Text;

    [Header("Car4 Stuff ----------------------")]
    public Button armorCar4Btn;
    public Text armorCar4Text;
    public Button weaponsCar4Btn;
    public Text weaponsCar4Text;
    public Button ccCar4Btn;
    public Text ccCar4Text;

    [Header("Other Buttons ---------------------")]
    public Button goBackBtn;

    private void updateValuesShopC1()
    {
        switch (gameManager.shopC1[0]) //shopC1[0] es la cantidad de vida del coche
        {
            case 100:
                armorCar1Text.text = "Lvl 1 | 500$";
                break;
            case 200:
                armorCar1Text.text = "Lvl 2 | 1000$";

                break;
            case 300:
                armorCar1Text.text = "Lvl 3 | MAX.";

                break;
        }

        switch (gameManager.shopC1[1]) //shopC1[1] es el nivel de las armas
        {
            case 1:
                weaponsCar1Text.text = "Lvl 1 | 500$";
                break;
            case 2:
                weaponsCar1Text.text = "Lvl 2 | 1000$";

                break;
            case 3:
                weaponsCar1Text.text = "Lvl 3 | MAX.";

                break;
        }

        switch (gameManager.shopC1[2]) //shopC1[2] es el cc del coche
        {
            case 500: //ACABAR DE DECIDIR VALORES DE CC!!!
                ccCar1Text.text = "Lvl 1 | 500$";
                break;
            case 550:
                ccCar1Text.text = "Lvl 2 | 1000$";

                break;
            case 650:
                ccCar1Text.text = "Lvl 3 | MAX.";

                break;
        }
    }

    private void updateValuesShopC2()
    {
        switch (gameManager.shopC2[0]) //shopC2[0] es la cantidad de vida del coche
        {
            case 100:
                armorCar2Text.text = "Lvl 1 | 500$";
                break;
            case 200:
                armorCar2Text.text = "Lvl 2 | 1000$";

                break;
            case 300:
                armorCar2Text.text = "Lvl 3 | MAX.";

                break;
        }

        switch (gameManager.shopC2[1]) //shopC2[1] es el nivel de las armas
        {
            case 1:
                weaponsCar2Text.text = "Lvl 1 | 500$";
                break;
            case 2:
                weaponsCar2Text.text = "Lvl 2 | 1000$";

                break;
            case 3:
                weaponsCar2Text.text = "Lvl 3 | MAX.";

                break;
        }

        switch (gameManager.shopC2[2]) //shopC2[2] es el cc del coche
        {
            case 500: //ACABAR DE DECIDIR VALORES DE CC!!!
                ccCar2Text.text = "Lvl 1 | 500$";
                break;
            case 550:
                ccCar2Text.text = "Lvl 2 | 1000$";

                break;
            case 650:
                ccCar2Text.text = "Lvl 3 | MAX.";

                break;
        }
    }

    private void updateValuesShopC3()
    {
        switch (gameManager.shopC3[0]) //shopC3[0] es la cantidad de vida del coche
        {
            case 100:
                armorCar3Text.text = "Lvl 1 | 500$";
                break;
            case 200:
                armorCar3Text.text = "Lvl 2 | 1000$";

                break;
            case 300:
                armorCar3Text.text = "Lvl 3 | MAX.";

                break;
        }

        switch (gameManager.shopC3[1]) //shopC3[1] es el nivel de las armas
        {
            case 1:
                weaponsCar3Text.text = "Lvl 1 | 500$";
                break;
            case 2:
                weaponsCar3Text.text = "Lvl 2 | 1000$";

                break;
            case 3:
                weaponsCar3Text.text = "Lvl 3 | MAX.";

                break;
        }

        switch (gameManager.shopC3[2]) //shopC3[2] es el cc del coche
        {
            case 500: //ACABAR DE DECIDIR VALORES DE CC!!!
                ccCar3Text.text = "Lvl 1 | 500$";
                break;
            case 550:
                ccCar3Text.text = "Lvl 2 | 1000$";

                break;
            case 650:
                ccCar3Text.text = "Lvl 3 | MAX.";

                break;
        }
    }

    private void updateValuesShopC4()
    {
        switch (gameManager.shopC4[0]) //shopC4[0] es la cantidad de vida del coche
        {
            case 100:
                armorCar4Text.text = "Lvl 1 | 500$";
                break;
            case 200:
                armorCar4Text.text = "Lvl 2 | 1000$";

                break;
            case 300:
                armorCar4Text.text = "Lvl 3 | MAX.";

                break;
        }

        switch (gameManager.shopC4[1]) //shopC4[1] es el nivel de las armas
        {
            case 1:
                weaponsCar4Text.text = "Lvl 1 | 500$";
                break;
            case 2:
                weaponsCar4Text.text = "Lvl 2 | 1000$";

                break;
            case 3:
                weaponsCar4Text.text = "Lvl 3 | MAX.";

                break;
        }

        switch (gameManager.shopC4[2]) //shopC4[2] es el cc del coche
        {
            case 500: //ACABAR DE DECIDIR VALORES DE CC!!!
                ccCar4Text.text = "Lvl 1 | 500$";
                
                break;
            case 550:
                ccCar4Text.text = "Lvl 2 | 1000$";
               

                break;
            case 650:
                ccCar4Text.text = "Lvl 3 | MAX.";

                break;
        }
    }


    private void buyArmorC1() //HACER UN MÉTODO POR CADA BOTÓN
    {

        switch (gameManager.shopC1[0]) //shopC1[0] es la vida del coche
        {
            case 100:
                if (gameManager.money >= 500)
                {
                    gameManager.money -= 500;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC1[0] = 200;
                    PlayerPrefs.SetInt("armorC1", gameManager.shopC1[0]);
                }

                break;
            case 200:
                if (gameManager.money >= 1000)
                {
                    gameManager.money -= 1000;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC1[0] = 300;
                    PlayerPrefs.SetInt("armorC1", gameManager.shopC1[0]);
                }

                break;

            case 300:
                armorCar1Text.text = "Lvl 3 | MAX.";

                break;
        }
    }
    private void buyWeaponsC1() //HACER UN MÉTODO POR CADA BOTÓN
    {

        switch (gameManager.shopC1[1]) //shopC1[1] es el lvl de armas
        {
            case 1: 
                if (gameManager.money >= 500)
                {
                    gameManager.money -= 500;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC1[1] = 2;
                    PlayerPrefs.SetInt("weaponsC1", gameManager.shopC1[1]);
                }

                break;
            case 2:
                if (gameManager.money >= 1000)
                {
                    gameManager.money -= 1000;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC1[1] = 3;
                    PlayerPrefs.SetInt("weaponsC1", gameManager.shopC1[1]);
                }
                break;

            case 3:
                weaponsCar1Text.text = "Lvl 3 | MAX.";

                break;
        }
    }
    private void buyCC1() //HACER UN MÉTODO POR CADA BOTÓN
    {

        switch (gameManager.shopC1[2]) //shopC4[2] es el cc del coche
        {
            case 500: //ACABAR DE DECIDIR VALORES DE CC!!!
                if (gameManager.money >= 500)
                {
                    gameManager.money -= 500;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC1[2] = 550;
                    PlayerPrefs.SetInt("ccC1", gameManager.shopC1[2]);
                }

                break;
            case 550:
                if (gameManager.money >= 1000)
                {
                    gameManager.money -= 1000;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC1[2] = 650;
                    PlayerPrefs.SetInt("ccC1", gameManager.shopC1[2]);
                }
                break;

            case 650:
                ccCar1Text.text = "Lvl 3 | MAX.";

                break;
        }
    }

    private void buyArmorC2() //HACER UN MÉTODO POR CADA BOTÓN
    {

        switch (gameManager.shopC2[0]) //shopC2[0] es la vida del coche
        {
            case 100:
                if (gameManager.money >= 500)
                {
                    gameManager.money -= 500;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC2[0] = 200;
                    PlayerPrefs.SetInt("armorC2", gameManager.shopC2[0]);
                }

                break;
            case 200:
                if (gameManager.money >= 1000)
                {
                    gameManager.money -= 1000;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC2[0] = 300;
                    PlayerPrefs.SetInt("armorC2", gameManager.shopC2[0]);
                }

                break;

            case 300:
                armorCar2Text.text = "Lvl 3 | MAX.";

                break;
        }
    }

    private void buyWeaponsC2() //HACER UN MÉTODO POR CADA BOTÓN
    {

        switch (gameManager.shopC2[1]) //shopC1[1] es el lvl de armas
        {
            case 1:
                if (gameManager.money >= 500)
                {
                    gameManager.money -= 500;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC2[1] = 2;
                    PlayerPrefs.SetInt("weaponsC2", gameManager.shopC2[1]);
                }

                break;
            case 2:
                if (gameManager.money >= 1000)
                {
                    gameManager.money -= 1000;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC2[1] = 3;
                    PlayerPrefs.SetInt("weaponsC2", gameManager.shopC2[1]);
                }
                break;

            case 3:
                weaponsCar2Text.text = "Lvl 3 | MAX.";

                break;
        }
    }



    private void buyCC2() //HACER UN MÉTODO POR CADA BOTÓN
    {

        switch (gameManager.shopC2[2]) //shopC4[2] es el cc del coche
        {
            case 500: //ACABAR DE DECIDIR VALORES DE CC!!!
                if (gameManager.money >= 500)
                {
                    gameManager.money -= 500;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC2[2] = 550;
                    PlayerPrefs.SetInt("ccC2", gameManager.shopC2[2]);
                }

                break;
            case 550:
                if (gameManager.money >= 1000)
                {
                    gameManager.money -= 1000;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC2[2] = 650;
                    PlayerPrefs.SetInt("ccC2", gameManager.shopC2[2]);
                }
                break;

            case 650:
                ccCar2Text.text = "Lvl 3 | MAX.";

                break;
        }
    }
    private void buyArmorC3() //HACER UN MÉTODO POR CADA BOTÓN
    {

        switch (gameManager.shopC3[0]) //shopC1[0] es la vida del coche
        {
            case 100:
                if (gameManager.money >= 500)
                {
                    gameManager.money -= 500;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC3[0] = 200;
                    PlayerPrefs.SetInt("armorC3", gameManager.shopC3[0]);
                }

                break;
            case 200:
                if (gameManager.money >= 1000)
                {
                    gameManager.money -= 1000;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC3[0] = 300;
                    PlayerPrefs.SetInt("armorC3", gameManager.shopC3[0]);
                }

                break;

            case 300:
                armorCar3Text.text = "Lvl 3 | MAX.";

                break;
        }
    }
    private void buyWeaponsC3() //HACER UN MÉTODO POR CADA BOTÓN
    {

        switch (gameManager.shopC3[1]) //shopC1[1] es el lvl de armas
        {
            case 1:
                if (gameManager.money >= 500)
                {
                    gameManager.money -= 500;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC3[1] = 2;
                    PlayerPrefs.SetInt("weaponsC3", gameManager.shopC3[1]);
                }

                break;
            case 2:
                if (gameManager.money >= 1000)
                {
                    gameManager.money -= 1000;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC3[1] = 3;
                    PlayerPrefs.SetInt("weaponsC3", gameManager.shopC3[1]);
                }
                break;

            case 3:
                weaponsCar3Text.text = "Lvl 3 | MAX.";

                break;
        }
    }

    private void buyCC3() //HACER UN MÉTODO POR CADA BOTÓN
    {

        switch (gameManager.shopC3[2]) //shopC4[2] es el cc del coche
        {
            case 500: //ACABAR DE DECIDIR VALORES DE CC!!!
                if (gameManager.money >= 500)
                {
                    gameManager.money -= 500;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC3[2] = 550;
                    PlayerPrefs.SetInt("ccC3", gameManager.shopC3[2]);
                }

                break;
            case 550:
                if (gameManager.money >= 1000)
                {
                    gameManager.money -= 1000;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC3[2] = 650;
                    PlayerPrefs.SetInt("ccC3", gameManager.shopC3[2]);
                }
                break;

            case 650:
                ccCar3Text.text = "Lvl 3 | MAX.";

                break;
        }
    }

    private void buyArmorC4() //HACER UN MÉTODO POR CADA BOTÓN
    {

        switch (gameManager.shopC4[0]) //shopC1[0] es la vida del coche
        {
            case 100:
                if (gameManager.money >= 500)
                {
                    gameManager.money -= 500;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC4[0] = 200;
                    PlayerPrefs.SetInt("armorC4", gameManager.shopC4[0]);
                }

                break;
            case 200:
                if (gameManager.money >= 1000)
                {
                    gameManager.money -= 1000;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC4[0] = 300;
                    PlayerPrefs.SetInt("armorC4", gameManager.shopC4[0]);
                }

                break;

            case 300:
                armorCar1Text.text = "Lvl 3 | MAX.";

                break;
        }
    }


    private void buyWeaponsC4() //HACER UN MÉTODO POR CADA BOTÓN
    {

        switch (gameManager.shopC4[1]) //shopC1[1] es el lvl de armas
        {
            case 1:
                if (gameManager.money >= 500)
                {
                    gameManager.money -= 500;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC4[1] = 2;
                    PlayerPrefs.SetInt("weaponsC4", gameManager.shopC4[1]);
                }

                break;
            case 2:
                if (gameManager.money >= 1000)
                {
                    gameManager.money -= 1000;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC4[1] = 3;
                    PlayerPrefs.SetInt("weaponsC4", gameManager.shopC4[1]);
                }
                break;

            case 3:
                weaponsCar4Text.text = "Lvl 3 | MAX.";

                break;
        }
    }

    private void buyCC4() //HACER UN MÉTODO POR CADA BOTÓN
    {
        
        switch (gameManager.shopC4[2]) //shopC4[2] es el cc del coche
        {
            case 500: //ACABAR DE DECIDIR VALORES DE CC!!!
                if (gameManager.money >= 500)
                {
                    gameManager.money -= 500;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC4[2] = 550;
                    PlayerPrefs.SetInt("ccC4", gameManager.shopC4[2]);
                }
                
                break;
            case 550:
                if (gameManager.money >= 1000)
                {
                    gameManager.money -= 1000;
                    PlayerPrefs.SetInt("money", gameManager.money);

                    gameManager.shopC4[2] = 650;
                    PlayerPrefs.SetInt("ccC4", gameManager.shopC4[2]);
                }
                break;

            case 650:
                ccCar4Text.text = "Lvl 3 | MAX.";

                break;
        }
    }
    // Start is called before the first frame update
    void Start()
    {


        // car1:
        goBackBtn.onClick.AddListener(goBack);
        armorCar1Btn.onClick.AddListener(buyArmorC1);
        armorCar2Btn.onClick.AddListener(buyArmorC2);
        armorCar3Btn.onClick.AddListener(buyArmorC3);
        armorCar4Btn.onClick.AddListener(buyArmorC4);
        weaponsCar1Btn.onClick.AddListener(buyWeaponsC1);
        weaponsCar2Btn.onClick.AddListener(buyWeaponsC2);
        weaponsCar3Btn.onClick.AddListener(buyWeaponsC3);
        weaponsCar4Btn.onClick.AddListener(buyWeaponsC4);
        ccCar1Btn.onClick.AddListener(buyCC1);
        ccCar2Btn.onClick.AddListener(buyCC2);
        ccCar3Btn.onClick.AddListener(buyCC3);
        ccCar4Btn.onClick.AddListener(buyCC4);



        /*
        armorCar1Btn.onClickAddListener(buyArmorCar1);
        weaponsCar1Btn.onClick.AddListener(buyWeaponsCar1);
        ccCar1Btn.onClick.AddListener(buyWeaponsCar1);
        */
    }
    void OnEnable()
    {
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
    }
    // Update is called once per frame
    void Update()
    {
        moneyText.text = gameManager.money + " $";
        updateValuesShopC1();
        updateValuesShopC2();
        updateValuesShopC3();
        updateValuesShopC4();
    }

    
    private void goBack()
    {
        this.gameObject.SetActive(false);
    }
    private void nonClickableBtn()
    {

    }
}
