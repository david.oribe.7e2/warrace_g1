﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CanvasYouDead : MonoBehaviour
{
    public Button continueBtn;
    GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        continueBtn.onClick.AddListener(continueNext);

    }

    // Update is called once per frame
    private void continueNext()
    {
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));

        gameManager.SendMessage("youDead");

    }
}
