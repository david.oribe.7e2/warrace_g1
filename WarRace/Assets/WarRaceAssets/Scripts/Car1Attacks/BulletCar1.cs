﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCar1 : MonoBehaviour
{
    // Start is called before the first frame update
    private float timeToDespawn;
    AudioSource audio;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        audio.Play();
    }

    // Update is called once per frame
    void Update()
    {
        timeToDespawn += Time.deltaTime;
        if (timeToDespawn>4)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && other.gameObject.tag != "triggerAttack")
        {
            vehiculo.life--;
            Destroy(this.gameObject);
        }
    }
}
