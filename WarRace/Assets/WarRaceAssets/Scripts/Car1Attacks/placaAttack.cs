﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class placaAttack : MonoBehaviour
{
    // Start is called before the first frame update

    private float timeToExplode;
    public int dmg = 3;

    //public GameObject explosionParticle;


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        timeToExplode += Time.deltaTime;
        if (timeToExplode > 7)
        {
            //GameObject explosion = Instantiate(explosionParticle, this.transform.position, this.transform.rotation);

            Destroy(this.gameObject);
        }

        
    }

    private void OnTriggerEnter(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && other.gameObject.tag != "triggerAttack")
        {
            vehiculo.life -= dmg;
            //GameObject explosion = Instantiate(explosionParticle, this.transform.position, this.transform.rotation);
            Destroy(this.gameObject);

        }
    }
}
