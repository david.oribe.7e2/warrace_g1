﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MisilCar2 : MonoBehaviour
{
    // Start is called before the first frame update

    private float timeToDespawn;
    public GameObject explosionParticle;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeToDespawn += Time.deltaTime;
        if (timeToDespawn > 4)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && other.gameObject.tag != "triggerAttack")
        {
            GameObject explosion = Instantiate(explosionParticle, this.transform.position, this.transform.rotation);

            vehiculo.life-=3;
            Destroy(this.gameObject);
        }
    }
}
