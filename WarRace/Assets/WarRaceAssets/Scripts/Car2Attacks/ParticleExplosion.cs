﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleExplosion : MonoBehaviour
{
    // Start is called before the first frame update
    private float timeToDespawn;
    new AudioSource audio;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        audio.Play();
    }

    // Update is called once per frame
    void Update()
    {
        timeToDespawn += Time.deltaTime;
        if (timeToDespawn > 4)
        {
            Destroy(this.gameObject);
        }
    }
}
