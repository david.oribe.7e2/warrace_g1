﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bombScript : MonoBehaviour
{
    // Start is called before the first frame update
    private float timeToStop;

    private float timeToExplode;
    public int dmg = 3;

    public GameObject explosionParticle;


    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

        timeToExplode += Time.deltaTime;
        if (timeToExplode > 7)
        {
            GameObject explosion = Instantiate(explosionParticle, this.transform.position, this.transform.rotation);

            Destroy(this.gameObject);
        }
        timeToStop += Time.deltaTime;

        if (timeToStop < 4) { 
        this.transform.Translate(new Vector3(0,  2 * Time.deltaTime,0));
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null&&timeToStop>4 && other.gameObject.tag != "triggerAttack")
        {
            vehiculo.life -= dmg;
            GameObject explosion = Instantiate(explosionParticle, this.transform.position, this.transform.rotation);
            Destroy(this.gameObject);

        }
    }
}
