﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularBladesScript : MonoBehaviour
{
    public GameObject particulas;
    // Start is called before the first frame update


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(0, 0, 2);
    }

    private void OnTriggerStay(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && other.gameObject.tag != "triggerAttack")
        {
            vehiculo.life -= 1;
            
        }
        GameObject particlula1 = Instantiate(particulas, this.transform.position, this.transform.rotation);

    }
}
