﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grapplingScript : MonoBehaviour
{
    // Start is called before the first frame update

    private float timeToDespawn;
    private bool isGrapped=false; //Boolean para que solo coja a un coche
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeToDespawn += Time.deltaTime;
        if (timeToDespawn > 5)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && other.gameObject.tag != "triggerAttack"&&!isGrapped)
        {
            vehiculo.isGrapped = true;
            this.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, -10);
            isGrapped = true;

        }
    }
}
