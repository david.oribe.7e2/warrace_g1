﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lanzallamas : MonoBehaviour
{
    public bool isShooting;
    public GameObject llamas;
    public float damageLlamas = 0.1f;
    public AudioClip llamasSonido;
    public AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        llamas.SetActive(false);
        isShooting = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && isShooting && other.gameObject.tag != "triggerAttack")
        {
            //QUITAR VIDA AQUÍ
            vehiculo.life -= 1;
            //Debug.Log("Quemando");
        }
    }

    public IEnumerator disparar()
    {
        //GameObject llamarada= Instantiate(llamas, initLlama.transform.position, llamas.transform.rotation);
        llamas.SetActive(true);
        
        isShooting = true;
        yield return new WaitForSeconds(5.0f);
        llamas.SetActive(false);
        
        isShooting = false;
    }

}
