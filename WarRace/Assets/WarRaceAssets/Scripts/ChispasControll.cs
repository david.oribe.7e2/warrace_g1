﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChispasControll : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(destruir());
    }

    public IEnumerator destruir()
    {
    
        yield return new WaitForSeconds(2.0f);
        Destroy(this.gameObject);
    }
}
