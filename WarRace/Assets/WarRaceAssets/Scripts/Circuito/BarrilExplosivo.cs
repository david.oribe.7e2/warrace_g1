﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class BarrilExplosivo : MonoBehaviour
{
    public float fuerzaDeImpulso;

    public GameObject explosionParticle;


    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && other.gameObject.tag != "triggerAttack")
        {
            vehiculo.life -= 3;
            // vechiculo.takeDamage();
            // vehiculo.AddForce(ForceMode.IMPULSE, Vector3.up * fuerzaDeImpulso);
            GameObject explosion = Instantiate(explosionParticle, this.transform.position, this.transform.rotation);

            Destroy(this.gameObject);
            
        }
    }
}
