﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;


public class MetaController : MonoBehaviour
{
    GameManager gameManager;
    public int maxVueltas;
    public int currentVuelta;
    public GameObject[] playerCars = new GameObject[4];
    public GameObject[] playerCanvas = new GameObject[4];
    public GameObject[] allCars = new GameObject[8];
    private GameObject selectedCar;
    public GameObject spawnPosition;
    public GameObject sector1;
    public GameObject sector2;
    public GameObject sector3;

    public bool startingRace = false;
    public bool finishedRace=false;
    public Text startRaceText;
    public new AudioSource audio;

    public List<Vehiculo> isDeathList = new List<Vehiculo>();
    public string[] cars = new string[8];
    public GameObject[] finishedCarsArray = new GameObject[8];
    GameObject finalCarreraCanvas;
    GameObject youDeadCanvas;
    GameObject pauseCanvas;

    private void waitToStartRace()
    {
        startingRace = true;
        for (int i = 0; i < allCars.Length; i++)
        {
            Vehiculo coche = allCars[i].GetComponent<Vehiculo>();
            coche.frenada = 100;
        }
        StartCoroutine(timeToStartCR());

    }

    IEnumerator timeToStartCR()
    {
        startRaceText.text = "";
        yield return new WaitForSeconds(1f);
        startRaceText.text = "3";
        audio.Play();
        yield return new WaitForSeconds(1f);
        startRaceText.text = "2";
        audio.Play();
        yield return new WaitForSeconds(1f);
        startRaceText.text = "1";
        audio.Play();
        yield return new WaitForSeconds(1f);
        startRaceText.text = "GO!";
        audio.pitch = 0.5f;
        audio.Play();
        //--------------
        for (int i = 0; i < allCars.Length; i++)
        {
            Vehiculo coche = allCars[i].GetComponent<Vehiculo>();
            coche.frenada = 0;
        }
        startingRace = false;
        startRace();
        yield return new WaitForSeconds(1f);
        startRaceText.text = "";
        

    }
    private float timeToRespawn=0;
    void Start()
    {
        Time.timeScale = 1;
        for (int i=0; i<cars.Length;i++)
        {
            cars[i] = "IN RACE";
        }
        finalCarreraCanvas = GameObject.FindGameObjectWithTag("CanvasFinalCarrera");
        youDeadCanvas = GameObject.FindGameObjectWithTag("CanvasYouDead");
        pauseCanvas = GameObject.FindGameObjectWithTag("CanvasPause");
        if (finalCarreraCanvas != null && youDeadCanvas!=null && pauseCanvas != null)
        {
            finalCarreraCanvas.SetActive(false);
            youDeadCanvas.SetActive(false);
            pauseCanvas.SetActive(false);
        }

        //-------------------------------------------
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
        
        selectedCar = selectCar();
        allCars[7] = selectedCar;
        if (gameManager.modeSelected == GameManager.ModeSelected.GrandPrix)
        {
            //displayLiveCars();
        }
        setCarInPosition(selectedCar);
        //--------------------------------
        waitToStartRace();
    }
    void Update()
    {

        if (Input.GetKey(KeyCode.R))
        {
            timeToRespawn += Time.deltaTime;
            if (timeToRespawn > 3)
            {
                if (selectedCar.GetComponent<Vehiculo>().sector1&&!selectedCar.GetComponent<Vehiculo>().sector2&&!selectedCar.GetComponent<Vehiculo>().sector3)
                {
                    selectedCar.transform.position = sector1.transform.position;
                    selectedCar.transform.rotation = sector1.transform.rotation;
                }
                else if (selectedCar.GetComponent<Vehiculo>().sector2&&!selectedCar.GetComponent<Vehiculo>().sector3)
                {
                    selectedCar.transform.position = sector2.transform.position;
                    selectedCar.transform.rotation = sector2.transform.rotation;
                }
                else if (selectedCar.GetComponent<Vehiculo>().sector3)
                {
                    selectedCar.transform.position = sector3.transform.position;
                    selectedCar.transform.rotation = sector3.transform.rotation;
                }
                else
                {
                    selectedCar.transform.position = spawnPosition.transform.position;
                    selectedCar.transform.rotation = spawnPosition.transform.rotation;
                }
                timeToRespawn = 0;
            }
        }
        Vehiculo coche = selectedCar.GetComponent<Vehiculo>();
        if (coche!=null&&coche.isDead)
        {
            youDeadCanvas.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseCanvas.SetActive(true);
            Time.timeScale = 0;
        }
        //sortCarPositions(); // GameObject[]allCars
        //lookForDeadCars(); // si se muere lo mete a List<Vehiculo>posicionFinal
        //sortListPosicionFinal(); //ordena los que se mueren atrás de List<Vehiculo>posicionFinals

    }
    /*
    public void sortCarPositions() //oredena la lista de allCars de los que consiguen terminar la carrera
    {
        for (int i = 0; i < allCars.Length - 1; i++)
        {
            GameObject cocheGO = allCars[i];
            Vehiculo coche = allCars[i].GetComponent<Vehiculo>();
            Vehiculo cocheAComparar = allCars[i + 1].GetComponent<Vehiculo>();
            if (coche.vueltaActual > maxVueltas && cocheAComparar != null)
            {
                if (coche.tiempoTotal > cocheAComparar.tiempoTotal)
                {
                    allCars[i] = allCars[i + 1];
                    allCars[i + 1] = cocheGO;
                }                
            }
            if (coche.isDead && cocheAComparar != null)
            {
                if (coche.isDead && !cocheAComparar.isDead)
                    allCars[i] = allCars[i + 1];
                    allCars[i + 1] = cocheGO;
            }
            if (coche.vueltaActual < maxVueltas && cocheAComparar != null)
            {
                if (cocheAComparar.vueltaActual > maxVueltas)
                {
                    allCars[i] = allCars[i + 1];
                    allCars[i + 1] = cocheGO;
                }
            }
        }
    }
    */
    /*
     * primero animación de algo de tiempo, luego llama a este método
     * recorre el array de todos los coches llamándoles al método startRace
     */
    private void startRace()
    {
        gameManager.gameState = GameManager.GameState.Playing;
        for (int i = 0; i < allCars.Length; i++)
        {
            Vehiculo coche = allCars[i].GetComponent<Vehiculo>();
            coche.StartRace();
        }
    }
    private void setCarInPosition(GameObject selectedCar)
    {
        selectedCar.transform.position = spawnPosition.transform.position;
    }
    private void displayLiveCars()
    {
        for (int i = 0; i < allCars.Length; i++)
        {
            Vehiculo coche = allCars[i].GetComponent<Vehiculo>();
            for (int j = 0; j < gameManager.classificationList.Length; j++)
            {
                if (gameManager.classificationList[j].Contains(coche.name) && gameManager.classificationList[j].Contains("DEAD"))
                {
                    coche.gameObject.SetActive(false);
                }
            }
            
        }
    }
    private GameObject selectCar()
    {
        GameObject selectedCar = null;
        for (int i = 0; i < playerCars.Length; i++)
        {
            playerCars[i].gameObject.SetActive(false);
            playerCanvas[i].gameObject.SetActive(false);
        }
        if (gameManager.car1)
        {
            playerCars[0].gameObject.SetActive(true);
            playerCanvas[0].gameObject.SetActive(true);
            selectedCar = playerCars[0].gameObject;

        }
        else if (gameManager.car2)
        {
            playerCars[1].gameObject.SetActive(true);
            playerCanvas[1].gameObject.SetActive(true);
            selectedCar = playerCars[1].gameObject;

        }
        else if (gameManager.car3)
        {
            playerCars[2].gameObject.SetActive(true);
            playerCanvas[2].gameObject.SetActive(true);
            selectedCar = playerCars[2].gameObject;

        }
        else if (gameManager.car4)
        {
            playerCars[3].gameObject.SetActive(true);
            playerCanvas[3].gameObject.SetActive(true);
            selectedCar = playerCars[3].gameObject;

        }
        //UnityEngine.Debug.Log(selectedCar.GetComponent<Vehiculo>().name);
        return selectedCar;
    }
    private void OnTriggerEnter(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && other.gameObject.tag != "triggerAttack")
        {            
            if (vehiculo.sector1 = true && vehiculo.sector2 == true && vehiculo.sector3 == true)
            {
                actualizarTiempo(vehiculo);
                sumarUnaVuelta(vehiculo);
                reinicarSectores(vehiculo);
            }  
        }
        PlayerController player = other.GetComponent<PlayerController>();
        if (player != null && vehiculo != null && other.gameObject.tag != "triggerAttack")
        {
            if (vehiculo.vueltaActual > maxVueltas)
            {
                //posicionFinal.Add(vehiculo);
                finalCarreraCanvas.SetActive(true);
                Time.timeScale = 4;
            }
        }

    }
    private void sortListPosicionFinal()
    {
        /*
        if (posicionFinal.Count > 0)
        {
            for (int i = 0; i < posicionFinal.Count; i++)
            {
                Vehiculo coche = posicionFinal[i];
                if (i < posicionFinal.Count)
                {
                    Vehiculo cocheAComparar = posicionFinal[i + 1];
                    if (coche.isDead && !cocheAComparar.isDead)
                    {
                        posicionFinal[i] = cocheAComparar;
                        posicionFinal[i + 1] = coche;
                    }
                }
            }
        }
        */
    }
    int currentDeaths = 8;
    private void lookForDeadCars()
    {
        
        for (int i = 0; i < allCars.Length; i++)
        {
            Vehiculo coche = allCars[i].GetComponent<Vehiculo>();
       
            if (coche.isDead && !isDeathList.Contains(coche))
            {
                currentDeaths--;

                isDeathList.Add(coche);
                finishedCarsArray[currentDeaths] = coche.gameObject;
                cars[currentDeaths] = coche.name + " | DEAD";
            }
        }
        
    }
    private void sumarUnaVuelta(Vehiculo vehiculo) {
        vehiculo.vueltaActual++;

        if (vehiculo.vueltaActual < maxVueltas)
        {
            currentVuelta = vehiculo.vueltaActual;
        }
        else if (vehiculo.vueltaActual > maxVueltas)
        {
            terminarCarrera(vehiculo);
        }
    }
    public int carsFinished = 0;
    private void terminarCarrera(Vehiculo vehiculo)
    {
        finishedRace = true;
        vehiculo.cc = 0;
        vehiculo.frenada = 5;
        cars[carsFinished] = vehiculo.name + " | " + System.Math.Round(vehiculo.tiempoTotal, 3) + " | " + vehiculo.puntos;
        finishedCarsArray[carsFinished] = vehiculo.gameObject;

        carsFinished++;
        //posicionFinal.Add(vehiculo); // añade los coches que van llegando a una lista a si  ya se la posicion de llegada directamente
    }

    private void reinicarSectores(Vehiculo vehiculo)
    {
        vehiculo.sector1 = false;
        vehiculo.sector2 = false;
        vehiculo.sector3 = false;
    }
    private void actualizarTiempo(Vehiculo vehiculo)
    {
        // actualiza el tiempo total de carrera
        vehiculo.tiempoTotal += vehiculo.tiempoVuelta;

        // actaliza el mejor tiempo
        if(vehiculo.tiempoVuelta < vehiculo.tiempoMejorVuelta)
        {
            vehiculo.tiempoMejorVuelta = vehiculo.tiempoVuelta;
        }
        vehiculo.tiempoAnteriorVuelta = vehiculo.tiempoVuelta;
        // reinicia el tiempo de vulta para la siguiente
        vehiculo.tiempoVuelta = 0; 
    }
}
