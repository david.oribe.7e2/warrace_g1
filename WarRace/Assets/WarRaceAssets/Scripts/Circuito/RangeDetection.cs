﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeDetection : MonoBehaviour
{
    TorretaHostil turretController;
    public GameObject bullet1; //bala torreta 


    void Start()
    {
        turretController = transform.parent.gameObject.GetComponent<TorretaHostil>();
    }
    private void OnTriggerStay(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && other.gameObject.tag != "triggerAttack")
        {
            // mirar hacia vehiculo
            turretController.turretHead.transform.LookAt(new Vector3(vehiculo.transform.position.x-5, vehiculo.transform.position.y, vehiculo.transform.position.z));
            
            if (turretController.canShoot)
            {
                GameObject bullet = Instantiate(bullet1, new Vector3(turretController.turretHead.transform.position.x, turretController.turretHead.transform.position.y+1.2f, turretController.turretHead.transform.position.z), turretController.turretHead.transform.rotation);
                Rigidbody rbBullet = bullet.GetComponent<Rigidbody>();
                rbBullet.velocity = turretController.turretHead.transform.TransformDirection(new Vector3(-20, 0, 0));
                //disparar
                turretController.canShoot = false;
                turretController.startTimeFireRate = 0;
            }
            
        }
    }
}
