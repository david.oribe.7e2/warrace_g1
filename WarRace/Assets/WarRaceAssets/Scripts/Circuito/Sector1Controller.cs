﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sector1Controller : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && other.gameObject.tag != "triggerAttack")
        {
            vehiculo.sector1 = true;
        }
    }
}
