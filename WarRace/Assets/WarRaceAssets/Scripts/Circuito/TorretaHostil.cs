﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorretaHostil : MonoBehaviour
{
    [Header("Partes de la Torreta -----------")]
    public GameObject turretHead;
    public Transform spawnShoot;
    public GameObject turretCannon;
    public GameObject rangeAttackTrigger;
    [Header("Turret Settings ------------")]
    public int health;
    public int damage;
    public float rangeAttack;
    public float fireRate=1;
    public float startTimeFireRate;
    public bool canShoot;

    // Start is called before the first frame update
    void Start()
    {
        canShoot = false;
        startTimeFireRate = 0;
        rangeAttackTrigger.GetComponent<SphereCollider>().radius = rangeAttack;
    }

    // Update is called once per frame
    void Update()
    {
        nextShoot();
    }
    
    private void nextShoot()
    {
        if (startTimeFireRate < fireRate)
        {
            startTimeFireRate += Time.deltaTime;
        }
        else if (fireRate <= startTimeFireRate)
        {
            canShoot = true;
        }
    }
}
