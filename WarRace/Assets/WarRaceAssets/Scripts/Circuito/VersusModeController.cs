﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VersusModeController : MonoBehaviour
{
    GameManager gameManager;
    public GameObject[] playerCars = new GameObject[4];
    public GameObject[] playerCanvas = new GameObject[4];
    public GameObject[] allCars = new GameObject[8];
    private GameObject selectedCar;
    public GameObject spawnPosition;

    public List<Vehiculo> isDeathList = new List<Vehiculo>();
    public GameObject[] finishedCarsArray = new GameObject[8];
    public string[] cars = new string[8];


    public bool startingRace;
    GameObject finalCarreraCanvas;
    GameObject youDeadCanvas;
    GameObject pauseCanvas;

    public Text startRaceText;
    public AudioSource audio;



    private void waitToStartRace()
    {
        startingRace = true;
        for (int i = 0; i < allCars.Length; i++)
        {
            Vehiculo coche = allCars[i].GetComponent<Vehiculo>();
            coche.frenada = 100;
        }
        StartCoroutine(timeToStartCR());

    }

    IEnumerator timeToStartCR()
    {
        startRaceText.text = "";
        yield return new WaitForSeconds(1f);
        startRaceText.text = "3";
        audio.Play();
        yield return new WaitForSeconds(1f);
        startRaceText.text = "2";
        audio.Play();
        yield return new WaitForSeconds(1f);
        startRaceText.text = "1";
        audio.Play();
        yield return new WaitForSeconds(1f);
        startRaceText.text = "GO!";
        audio.pitch = 0.5f;
        audio.Play();
        for (int i = 0; i < allCars.Length; i++)
        {
            Vehiculo coche = allCars[i].GetComponent<Vehiculo>();
            allCars[i].SetActive(true);

            coche.frenada = 0;
        }
        startingRace = false;
        yield return new WaitForSeconds(1f);
        startRaceText.text = "";

    }
    // Start is called before the first frame update
    void Start()
    {
        desactivateCars();
           finalCarreraCanvas = GameObject.FindGameObjectWithTag("CanvasFinalCarrera");
        youDeadCanvas = GameObject.FindGameObjectWithTag("CanvasYouDead");
        pauseCanvas = GameObject.FindGameObjectWithTag("CanvasPause");
        if (finalCarreraCanvas != null && youDeadCanvas != null && pauseCanvas != null)
        {
            finalCarreraCanvas.SetActive(false);
            youDeadCanvas.SetActive(false);
            pauseCanvas.SetActive(false);
        }

        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));

        selectedCar = selectCar();
        allCars[7] = selectedCar;
        setCarInPosition(selectedCar);
        waitToStartRace();
    }

    private void desactivateCars()
    {
        for (int i=0; i<allCars.Length-1;i++)
        {
            allCars[i].SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        Vehiculo coche = selectedCar.GetComponent<Vehiculo>();
        if (coche != null && coche.isDead)
        {
            youDeadCanvas.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseCanvas.SetActive(true);
            Time.timeScale = 0;
        }
        lookForDeadCars();
        endGame();
    }
    private void endGame()
    {
        Vehiculo cochePlayer = selectedCar.GetComponent<Vehiculo>();
        if (cochePlayer.isDead || finishedCarsArray[1]!=null)
        {
            finalCarreraCanvas.SetActive(true);
        }

    }
    public int currentDeaths = 8;
    private void lookForDeadCars()
    {

        for (int i = 0; i < allCars.Length; i++)
        {
            Vehiculo coche = allCars[i].GetComponent<Vehiculo>();

            if (coche.isDead && !isDeathList.Contains(coche))
            {
                currentDeaths--;

                isDeathList.Add(coche);
                finishedCarsArray[currentDeaths] = coche.gameObject;
                cars[currentDeaths] = coche.name + " | DEAD";
            }else if (!coche.isDead && !isDeathList.Contains(coche))
            {
                cars[0] = coche.name + " | " + "100 $";

            }
        }

    }
    private void setCarInPosition(GameObject selectedCar)
    {
        selectedCar.transform.position = spawnPosition.transform.position;
    }

    private GameObject selectCar()
    {
        GameObject selectedCar = null;
        for (int i = 0; i < playerCars.Length; i++)
        {
            playerCars[i].gameObject.SetActive(false);
            playerCanvas[i].gameObject.SetActive(false);
        }
        if (gameManager.car1)
        {
            playerCars[0].gameObject.SetActive(true);
            playerCanvas[0].gameObject.SetActive(true);
            selectedCar = playerCars[0].gameObject;

        }
        else if (gameManager.car2)
        {
            playerCars[1].gameObject.SetActive(true);
            playerCanvas[1].gameObject.SetActive(true);
            selectedCar = playerCars[1].gameObject;

        }
        else if (gameManager.car3)
        {
            playerCars[2].gameObject.SetActive(true);
            playerCanvas[2].gameObject.SetActive(true);
            selectedCar = playerCars[2].gameObject;

        }
        else if (gameManager.car4)
        {
            playerCars[3].gameObject.SetActive(true);
            playerCanvas[3].gameObject.SetActive(true);
            selectedCar = playerCars[3].gameObject;

        }
        //UnityEngine.Debug.Log(selectedCar.GetComponent<Vehiculo>().name);
        return selectedCar;
    }
}
