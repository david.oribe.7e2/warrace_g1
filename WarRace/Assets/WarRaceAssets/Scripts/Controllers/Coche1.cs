﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coche1 : Vehiculo
{
    //TODO Pillar el boolean attackActive para saber si se puede atacar y el int attack selected para saber que ataque estamos haciendo de la clase AttackController
    // Start is called before the first frame update
    public PlayerController playerController;
    GameManager gameManager;

    [Header("Bullets Settings ------------------")]
    public float bulletSpeed = 20f; //velocidad de las balas

    public GameObject bullet1; //bala torreta 
    public GameObject initShoot1; //posición inicial torreta 2
    public GameObject initShoot2; //posición inicial torreta 2

    [Header("Placa Settings ------------------")]
    //public GameObject trampillaObj;
    public GameObject Placa;

    public GameObject initPlaca;

    private bool canAttack2;
    public GameObject explosionDeadParticle;



    void Start()
    {
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));

        canAttack2 = false;
        life = gameManager.shopC1[0];
        maxLife = gameManager.shopC1[0];

        lvlArmas = gameManager.shopC1[1];
        cc = gameManager.shopC1[2];
        

    }

    // Update is called once per frame
    void Update()
    {

        if (canAttack2)
        {
            if (lvlArmas == 1)
            {
                lanzarPlaca();
            }
            else
            {
                ataquePlacaLvl2();
            }
        }
        if (life <= 0)
        {
            this.Dead();
        }
        if (raceing)
        {
            tiempoVuelta += Time.deltaTime;
        }

    }

    public override void Attack()
    {
        if (attackActive)
        {
            if (playerController.attackSelected == 1)
            {
                StartCoroutine(AttackOnecoroutine());
                //Debug.Log("Ataque 1!!");
            }
            else
            {
                canAttack2 = true;

                //Debug.Log("Ataque 2!!");
            }
        }
        //hacer un if cogiendo el attackSelected de la clase AttackController y según que ataque seleccione el jugador hará un ataque u otro.
    }

    private void makeOneShoot()
    {
        GameObject bullet = Instantiate(bullet1, initShoot1.transform.position, initShoot1.transform.rotation);
        Rigidbody rbBullet = bullet.GetComponent<Rigidbody>();
        rbBullet.velocity = initShoot1.transform.TransformDirection(new Vector3(-bulletSpeed, 0, 0));

        GameObject bullet2 = Instantiate(bullet1, initShoot2.transform.position, initShoot2.transform.rotation);
        Rigidbody rbBullet2 = bullet2.GetComponent<Rigidbody>();
        rbBullet2.velocity = initShoot2.transform.TransformDirection(new Vector3(-bulletSpeed, 0, 0));
    }
    IEnumerator AttackOnecoroutine()
    {
        if (lvlArmas == 1||lvlArmas==2)
        {
            makeOneShoot();
            yield return new WaitForSeconds(0.3f);
            makeOneShoot();
            yield return new WaitForSeconds(0.3f);
            makeOneShoot();
        }else
        {
            makeOneShoot();
            yield return new WaitForSeconds(0.3f);
            makeOneShoot();
            yield return new WaitForSeconds(0.3f);
            makeOneShoot();
            yield return new WaitForSeconds(0.3f);
            makeOneShoot();
            yield return new WaitForSeconds(0.3f);
            makeOneShoot();
        }
    }
    private void lanzarPlaca()
    {
        
            GameObject placa = Instantiate(Placa, initPlaca.transform.position, initPlaca.transform.rotation);
            canAttack2 = false;
        
       
    }

    private void ataquePlacaLvl2()
    {
        GameObject placa = Instantiate(Placa, initPlaca.transform.position, initPlaca.transform.rotation);
        GameObject placa2 = Instantiate(Placa, initPlaca.transform.position, initPlaca.transform.rotation);
        canAttack2 = false;



    }
    public override void Dead()
    {
        StartCoroutine(AnimDead());
        //Destroy(this.gameObject);
    }

    IEnumerator AnimDead()
    {
        GameObject explosionDead = Instantiate(explosionDeadParticle, this.transform.position, this.transform.rotation);
        yield return new WaitForSeconds(1f);
        isDead = true;

        this.gameObject.SetActive(false);

    }


}
