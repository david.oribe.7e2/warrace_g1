﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coche2 : Vehiculo
{
    // Start is called before the first frame update

    public PlayerController playerController;
    GameManager gameManager;
    [Header("Misil Settings ------------------")]
    public float misilSpeed = 20f; //velocidad del misil

    public GameObject misil1; //bala misil 
    public GameObject initShoot1; //posición inicial rpg

    [Header("Bomba Settings ------------------")]
    //public GameObject trampillaObj;
    public GameObject Bomb;

    public GameObject initBomb;

    private bool canAttack2;

    public GameObject smokeParticle;
    public GameObject explosionDeadParticle;

    public AudioSource audioSource;

    void Start()
    {
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));

        canAttack2 = false;

        life = gameManager.shopC2[0];
        maxLife = gameManager.shopC2[0];

        lvlArmas = gameManager.shopC2[1];
        cc = gameManager.shopC2[2];
    }

    // Update is called once per frame
    void Update()
    {
        //graphicalLife(); AÚN NO VA BIEN


        if (canAttack2)
        {
            if (lvlArmas == 1)
            {
                letBomb();
            }
            else if(lvlArmas==2)
            {
                ataqueBombaLvl2();
            }
            else
            {
                ataqueBombaLvl3();
            }
        }
        if (life <= 0)
        {
            this.Dead();
        }
        if (raceing)
        {
            tiempoVuelta += Time.deltaTime;
        }
    }

    private void graphicalLife() //método que instancia particulas de humo según  la vida del coche
    {
        if (life <= 50)
        {
            GameObject smoke = Instantiate(smokeParticle, initBomb.transform.position, this.transform.rotation);

        }
        else if(life <= 25){
            GameObject smoke = Instantiate(smokeParticle, initShoot1.transform.position, this.transform.rotation);

        }
    }
    public override void Attack()
    {
        if (attackActive)
        {
            if (playerController.attackSelected == 1)
            {
                audioSource.Play();
                GameObject misil = Instantiate(misil1, initShoot1.transform.position, initShoot1.transform.rotation);
                Rigidbody rbMisil = misil.GetComponent<Rigidbody>();
                rbMisil.velocity = initShoot1.transform.TransformDirection(new Vector3(misilSpeed, 0, 0));
                //Debug.Log("Ataque 1!!");
            }
            else
            {
                canAttack2 = true;
                //Debug.Log("Ataque 2!!");
            }
        }
        //hacer un if cogiendo el attackSelected de la clase AttackController y según que ataque seleccione el jugador hará un ataque u otro.
    }
    float bombTime;
    private void letBomb()
    {

        //if (trampillaObj.transform.rotation.x > -0.5)
        //{
         //   trampillaObj.transform.Rotate(-5, 0, 0);
       // }
        //else
       // {
            //trampillaObj.transform.Rotate(0, 0, 0);
            GameObject bomb = Instantiate(Bomb, initBomb.transform.position, initBomb.transform.rotation);
            canAttack2 = false;

       // }

    }

    private void ataqueBombaLvl2()
    {
        GameObject bomb = Instantiate(Bomb, initBomb.transform.position, initBomb.transform.rotation);
        GameObject bomb2 = Instantiate(Bomb, initBomb.transform.position, initBomb.transform.rotation);
        canAttack2 = false;
    }

    private void ataqueBombaLvl3()
    {
        GameObject bomb = Instantiate(Bomb, initBomb.transform.position, initBomb.transform.rotation);
        GameObject bomb2 = Instantiate(Bomb, initBomb.transform.position, initBomb.transform.rotation);
        GameObject bomb3 = Instantiate(Bomb, initBomb.transform.position, initBomb.transform.rotation);
        canAttack2 = false;
    }

    public override void Dead()
    {
        StartCoroutine(AnimDead());
        //Destroy(this.gameObject);
    }

    IEnumerator AnimDead()
    {
        GameObject explosionDead = Instantiate(explosionDeadParticle, this.transform.position, this.transform.rotation);
        yield return new WaitForSeconds(1f);
        isDead = true;
        this.gameObject.SetActive(false);
       
    }


}
