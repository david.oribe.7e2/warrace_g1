﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coche3 : Vehiculo
{
    //TODO Pillar el boolean attackActive para saber si se puede atacar y el int attack selected para saber que ataque estamos haciendo de la clase AttackController
    // Start is called before the first frame update
    public PlayerController playerController;
    public GameObject grapplingObj;
    public GameObject initGrap;

    public GameObject grapplingSprite;//grappling que siempre estará en el coche y que desactivaremos cuando ataque 

    public float grapplingSpeed = 20f;
    public GameObject explosionDeadParticle;
    GameManager gameManager;

    public GameObject saw1;
    public GameObject saw2;
    public GameObject saw3;
    public GameObject saw4;

    void Start()
    {
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));



        life = gameManager.shopC3[0];
        maxLife = gameManager.shopC3[0];

        lvlArmas = gameManager.shopC3[1];
        cc = gameManager.shopC3[2];
    }

    // Update is called once per frame
    void Update()
    {
        if (lvlArmas==1)
        {
            saw1.SetActive(true);
            saw2.SetActive(true);
            saw3.SetActive(false);
            saw4.SetActive(false);
        }else if (lvlArmas==2)
        {
            saw1.SetActive(true);
            saw2.SetActive(true);
            saw3.SetActive(true);
            saw4.SetActive(false);
        }
        else {
            saw1.SetActive(true);
            saw2.SetActive(true);
            saw3.SetActive(true);
            saw4.SetActive(true);
        }
        if (raceing)
        {
            tiempoVuelta += Time.deltaTime;
        }
        if (attackActive)
        {
            grapplingSprite.SetActive(true);
        }
        else
        {
            grapplingSprite.SetActive(false);

        }

        if (life <= 0)
        {
            this.Dead();
        }
    }
    public override void Attack()
    {
        if (attackActive)
        {
            if (playerController.attackSelected == 1)
            {
                Debug.Log("Ataque 1!!");
                grapplingAttack();
            }
            else
            {
                Debug.Log("Ataque 2!!");
                grapplingAttack();

            }
        }
        //hacer un if cogiendo el attackSelected de la clase AttackController y según que ataque seleccione el jugador hará un ataque u otro.
    }

  
    void grapplingAttack()
    {
        grapplingSprite.SetActive(false);
        GameObject grappling = Instantiate(grapplingObj, initGrap.transform.position, initGrap.transform.rotation);
        Rigidbody rbGrap = grappling.GetComponent<Rigidbody>();
        rbGrap.velocity = initGrap.transform.TransformDirection(new Vector3(0, grapplingSpeed, 0));

    }

    public override void Dead()
    {
        StartCoroutine(AnimDead());
        //Destroy(this.gameObject);
    }

    IEnumerator AnimDead()
    {
        GameObject explosionDead = Instantiate(explosionDeadParticle, this.transform.position, this.transform.rotation);
        yield return new WaitForSeconds(1f);
        isDead = true;

        this.gameObject.SetActive(false);

    }
}
