﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coche4 : Vehiculo
{
    //TODO Pillar el boolean attackActive para saber si se puede atacar y el int attack selected para saber que ataque estamos haciendo de la clase AttackController
    // Start is called before the first frame update
    public PlayerController playerController;
    public Lanzallamas lanzallamas;

    public Collider lanzallamasTrigger;

    [Header("Spikes Settings ------------------")]
    //public GameObject trampillaObj;
    public GameObject Spikes;

    public GameObject initSpikes;

    private bool canAttack2;
    public GameObject explosionDeadParticle;
    GameManager gameManager;


    void Start()
    {
        lanzallamasTrigger.enabled = false;

        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));

        canAttack2 = false;

        life = gameManager.shopC4[0];
        maxLife = gameManager.shopC4[0];

        lvlArmas = gameManager.shopC4[1];
        cc = gameManager.shopC4[2];

    }

    // Update is called once per frame
    void Update()
    {
        if (raceing)
        {
            tiempoVuelta += Time.deltaTime;
        }
        if (canAttack2)
        {
            if (lvlArmas==1) {
                lanzarPinchos();
            }else if (lvlArmas==2)
            {
                lanzarPinchosLvl2();
            }
            else
            {
                lanzarPinchosLvl3();
            }
        }
        if (life <= 0)
        {
            this.Dead();
        }
    }
    public override void Attack()
    {
        if (attackActive)
        {
            if (playerController.attackSelected == 1)
            {
                //Debug.Log("Ataque 1!!");
                lanzallamasTrigger.enabled = true;

                StartCoroutine(lanzallamas.disparar());
            }
            else
            {

                canAttack2 = true;

                //Debug.Log("Ataque 2!!");
            }
        }
        else
        {
            lanzallamasTrigger.enabled = false;
        }
        //hacer un if cogiendo el attackSelected de la clase AttackController y según que ataque seleccione el jugador hará un ataque u otro.
    }
    private void lanzarPinchos()
    {


        GameObject spikes = Instantiate(Spikes, initSpikes.transform.position, Spikes.transform.rotation);
        canAttack2 = false;

        // }

    }

    private void lanzarPinchosLvl2()
    {


        GameObject spikes = Instantiate(Spikes, initSpikes.transform.position, Spikes.transform.rotation);
        GameObject spikes2 = Instantiate(Spikes, initSpikes.transform.position, Spikes.transform.rotation);
        canAttack2 = false;

        // }

    }

    private void lanzarPinchosLvl3()
    {


        GameObject spikes = Instantiate(Spikes, initSpikes.transform.position, Spikes.transform.rotation);
        GameObject spikes2 = Instantiate(Spikes, initSpikes.transform.position, Spikes.transform.rotation);
        GameObject spikes3 = Instantiate(Spikes, initSpikes.transform.position, Spikes.transform.rotation);
        canAttack2 = false;

        // }

    }
    public override void Dead()
    {
        StartCoroutine(AnimDead());
        //Destroy(this.gameObject);
    }

    IEnumerator AnimDead()
    {
        GameObject explosionDead = Instantiate(explosionDeadParticle, this.transform.position, this.transform.rotation);
        yield return new WaitForSeconds(1f);
        isDead = true;

        this.gameObject.SetActive(false);

    }
}
