﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HabilityController : MonoBehaviour
{
    // Start is called before the first frame update
    Image timeBar;
    public float habilityChargeTime = 10f;
    private float timeLeft;
    public GameObject isChargedText;
    void Start()
    {
        isChargedText.SetActive(false);
        timeBar = GetComponent<Image>();
        //timeLeft = habilityChargeTime;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (timeLeft < habilityChargeTime) //También pondremos que si vas haciendo daño se va cargando algo más rápido.
        {
            timeLeft += Time.deltaTime;
            timeBar.fillAmount = timeLeft / habilityChargeTime;
        }
        else
        {
            isChargedText.SetActive(true);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                isChargedText.SetActive(false);
                //HACER HABILIDAD EN CUESTION con un switch detectar el coche
                timeBar.fillAmount = 0;
                timeLeft = 0;
            }
        }
    }
}
