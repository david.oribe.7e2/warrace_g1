﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    GameManager gameManager;
    Rigidbody _rb;
    public Vehiculo vehiculo;
    [Header("Car Settings ------------------")]
    public Vector3 centerOfMass = Vector3.zero;
    public float motorForce;
    public float brakeForce;
    public float maxSteerAngle;
    public WheelCollider ruedaDDCollider;
    public WheelCollider ruedaDICollider;

    [Header("Attack Settings ---------------")]
    //public Image isAttackActive;
    //public Image attack1;
    //public Image attack2;
    public int attackSelected;
    private bool isAttackChanged;

    public Image spriteAttack1;
    public Image spriteAttack2;

    public float timeToQuitGrap = 0;

    MetaController metaController;
    VersusModeController versusController;

    void FixedUpdate()
    {
        // dar fuerza a las ruedas para tirar hacia adelante
        motorForce = Input.GetAxis("Vertical") * vehiculo.cc;
        ruedaDDCollider.motorTorque = motorForce;
        ruedaDICollider.motorTorque = motorForce;

        brakeForce = vehiculo.frenada;
        ruedaDDCollider.brakeTorque = brakeForce;
        ruedaDDCollider.brakeTorque = brakeForce;

        // para que las ruedas giren el coche
        float rotation = Input.GetAxis("Horizontal") * maxSteerAngle;
        ruedaDDCollider.steerAngle = rotation;
        ruedaDICollider.steerAngle = rotation;
        //para que las ruedas giren gráficamente
        ruedaDDCollider.transform.localEulerAngles = new Vector3(0f, rotation, 0f);
        ruedaDICollider.transform.localEulerAngles = new Vector3(0f, rotation, 0f);

    }
    // Start is called before the first frame update
    void Start()
    {
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
        vehiculo.isGrapped = false;

        spriteAttack1.enabled = false;
        spriteAttack2.enabled = false;


        vehiculo = this.gameObject.GetComponent<Vehiculo>();

        _rb = this.gameObject.GetComponent<Rigidbody>();
        _rb.centerOfMass = centerOfMass;
        //-------------------------------------
        attackSelected = 1;
        isAttackChanged = false;

        metaController = (MetaController)FindObjectOfType(typeof(MetaController));
        versusController = (VersusModeController)FindObjectOfType(typeof(VersusModeController));


    }

    // Update is called once per frame
    void Update()
    {
        
        if (vehiculo.isGrapped)
        {
            timeToQuitGrap += Time.deltaTime;
            vehiculo.frenada = 10;
            transform.Translate(new Vector3(0, 0, -15 * Time.deltaTime));
            if (timeToQuitGrap > 3)
            {
                vehiculo.isGrapped = false;
            }
        }
        else if (gameManager.modeSelected == GameManager.ModeSelected.OneRace|| gameManager.modeSelected == GameManager.ModeSelected.GrandPrix)
        {
            if (!vehiculo.isGrapped && !metaController.startingRace && !metaController.finishedRace && !Input.GetKey(KeyCode.LeftShift))
            {
                vehiculo.frenada = 0;
                transform.Translate(new Vector3(0, 0, 0));
            }
        }


        else if (gameManager.modeSelected == GameManager.ModeSelected.Versus)
        {
            if (!vehiculo.isGrapped && !versusController.startingRace && !Input.GetKey(KeyCode.LeftShift))
            {
                vehiculo.frenada = 0;
                transform.Translate(new Vector3(0, 0, 0));
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            isAttackChanged = !isAttackChanged;

            //UnityEngine.Debug.Log("changed Attack" + attackSelected);

        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            vehiculo.frenada = 10;

        }

        if (isAttackChanged)
        {
            attackSelected = 2;
            
        }
        else
        {
            attackSelected = 1;
            
        }

        if (vehiculo.attackActive)
        {
            if (attackSelected == 1)
            {
                spriteAttack1.enabled = true;
                spriteAttack2.enabled = false;
            }else
            {
                spriteAttack1.enabled = false;
                spriteAttack2.enabled = true;
            }
            if (Input.GetKeyDown(KeyCode.Mouse0))//También hará que poner como condición si el boolean attackActive estará activado. 
            {

                vehiculo.Attack();
                vehiculo.attackActive = false;
            }
        }
        else
        {
            spriteAttack1.enabled = false;
            spriteAttack2.enabled = false;

        }
    }
}
