﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehiculo : MonoBehaviour
{
    GameManager gameManager;
    [Header("LvL Settings --------------------")]
    public int cc; //Centímetros Cúbicos del vehículo SE PODRÁ MEJORAR EN LA TIENDA
    //public int lvlBlindaje; //nivel de blindaje del vehiculo(Máx. 3) 
    public int lvlArmas; //Nivel de las armas(Máx. 3) SE PODRÁ MEJORAR EN LA TIENDA
    public int frenada;

    [Header("Car Settings --------------------")]
    public int id; //id del vehiculo
    public string name; //nombre del vehiculo o conductor
    public int life; //Current vida del coche SE PODRÁ MEJORAR EN LA TIENDA
    public int maxLife;//Vida mmáxima que puede tener el coche que dependerá del blindaje
    public bool isDead = false;
    public bool attackActive = false;
    public bool isGrapped=false;

    [Header("Race Settings --------------------")]
    public int puntos;
    public int posicion;
    public int vueltaActual;
    public bool sector1;
    public bool sector2;
    public bool sector3;
    public float tiempoTotal = 0;
    public float tiempoVuelta = 0;
    public float tiempoAnteriorVuelta = 0;
    public float tiempoMejorVuelta = 12000f;
    public bool raceing = false;

    public virtual void Dead() 
    {
        
    }    

    public virtual void Attack() //Método que heredaran las clases hijas para hacer un ataque determinado cuando el coche pueda atacar y el jugador o la IA decida atacar.
    {
    }
    public void StartRace()
    {
        // lo tendra que llamar un manager cuando empieze la carrera(cuando los coches ya puedan correr).
        raceing = true;
        tiempoVuelta = 0;
        vueltaActual = 1;
        //UnityEngine.Debug.Log(name + " Start Race");

    }



}
