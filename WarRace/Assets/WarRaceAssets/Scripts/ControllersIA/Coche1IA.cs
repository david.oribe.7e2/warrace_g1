﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coche1IA : Vehiculo
{
    [Header("Bullets Settings ------------------")]
    public float bulletSpeed = 20f; //velocidad de las balas

    public GameObject bullet1; //bala torreta 
    public GameObject initShoot1; //posición inicial torreta 2
    public GameObject initShoot2; //posición inicial torreta 2

    [Header("Placa Settings ------------------")]
    //public GameObject trampillaObj;
    public GameObject Placa;

    public GameObject initPlaca;


    private float timeToAttack1=0;
    public GameObject explosionDeadParticle;
    GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));

        lvlArmas = gameManager.shopC1[1];

    }

    // Update is called once per frame
    void Update()
    {
        if (attackActive)
        {
            timeToAttack1 += Time.deltaTime;
            if (timeToAttack1 > 5)
            {
                if (lvlArmas == 1)
                {
                    lanzarPlaca();
                }
                else
                {
                    ataquePlacaLvl2();
                }
                attackActive = false;
            }
        }
        else
        {
            timeToAttack1 = 0;
        }

        if (life <= 0)
        {
            this.Dead();
        }
        if (raceing)
        {
            tiempoVuelta += Time.deltaTime;
        }
    }

    public override void Dead()
    {
        isDead = true;
        StartCoroutine(AnimDead());
        //Destroy(this.gameObject);
    }

    IEnumerator AnimDead()
    {
        GameObject explosionDead = Instantiate(explosionDeadParticle, this.transform.position, this.transform.rotation);
        yield return new WaitForSeconds(0.3f);
        this.gameObject.SetActive(false);

    }

    private void OnTriggerEnter(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && other.gameObject.tag != "triggerAttack")
        {
            if (attackActive&&timeToAttack1<5)
            {
                //grapplingSprite.SetActive(false);
                StartCoroutine(AttackOnecoroutine());
                attackActive = false;


            }
            


        }
    }

    private void lanzarPlaca()
    {


        GameObject placa = Instantiate(Placa, initPlaca.transform.position, initPlaca.transform.rotation);

        // }

    }
    private void ataquePlacaLvl2()
    {
        GameObject placa = Instantiate(Placa, initPlaca.transform.position, initPlaca.transform.rotation);
        GameObject placa2 = Instantiate(Placa, initPlaca.transform.position, initPlaca.transform.rotation);



    }

    private void makeOneShoot()
    {
        GameObject bullet = Instantiate(bullet1, initShoot1.transform.position, initShoot1.transform.rotation);
        Rigidbody rbBullet = bullet.GetComponent<Rigidbody>();
        rbBullet.velocity = initShoot1.transform.TransformDirection(new Vector3(-bulletSpeed, 0, 0));

        GameObject bullet2 = Instantiate(bullet1, initShoot2.transform.position, initShoot2.transform.rotation);
        Rigidbody rbBullet2 = bullet2.GetComponent<Rigidbody>();
        rbBullet2.velocity = initShoot2.transform.TransformDirection(new Vector3(-bulletSpeed, 0, 0));
    }
    IEnumerator AttackOnecoroutine()
    {
        if (lvlArmas == 1 || lvlArmas == 2)
        {
            makeOneShoot();
            yield return new WaitForSeconds(0.5f);
            makeOneShoot();
            yield return new WaitForSeconds(0.5f);
            makeOneShoot();
        }
        else
        {
            makeOneShoot();
            yield return new WaitForSeconds(0.5f);
            makeOneShoot();
            yield return new WaitForSeconds(0.5f);
            makeOneShoot();
            yield return new WaitForSeconds(0.5f);
            makeOneShoot();
            yield return new WaitForSeconds(0.5f);
            makeOneShoot();
        }

    }
}
