﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coche2IA : Vehiculo
{
    // Start is called before the first frame update
    [Header("Misil Settings ------------------")]
    public float misilSpeed = 20f; //velocidad del misil

    public GameObject misil1; //bala misil 
    public GameObject initShoot1; //posición inicial rpg

    [Header("Bomba Settings ------------------")]
    //public GameObject trampillaObj;
    public GameObject Bomb;

    public GameObject initBomb;
    private float timeToAttack1 = 0;
    public GameObject explosionDeadParticle;
    GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
        lvlArmas = gameManager.shopC2[1];


    }

    // Update is called once per frame
    void Update()
    {
        if (raceing)
        {
            tiempoVuelta += Time.deltaTime;
        }
        if (attackActive)
        {
            timeToAttack1 += Time.deltaTime;
            if (timeToAttack1 > 5)
            {
                //lanzarPlaca();
                if (lvlArmas == 1)
                {
                    letBomb();
                }
                else if (lvlArmas == 2)
                {
                    ataqueBombaLvl2();
                }
                else
                {
                    ataqueBombaLvl3();
                }
                attackActive = false;
            }
        }
        else
        {
            timeToAttack1 = 0;
        }

        if (life <= 0)
        {
            this.Dead();
        }
    }

    public override void Dead()
    {
        isDead = true;
        StartCoroutine(AnimDead());
        //Destroy(this.gameObject);
    }

    IEnumerator AnimDead()
    {
        GameObject explosionDead = Instantiate(explosionDeadParticle, this.transform.position, this.transform.rotation);
        yield return new WaitForSeconds(0.3f);
        this.gameObject.SetActive(false);

    }

    private void OnTriggerEnter(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && other.gameObject.tag != "triggerAttack")
        {
            if (attackActive && timeToAttack1 < 5)
            {

                GameObject misil = Instantiate(misil1, initShoot1.transform.position, initShoot1.transform.rotation);
                Rigidbody rbMisil = misil.GetComponent<Rigidbody>();
                rbMisil.velocity = initShoot1.transform.TransformDirection(new Vector3(misilSpeed, 0, 0));
                attackActive = false;


            }



        }
    }

    private void letBomb()
    {

        GameObject bomb = Instantiate(Bomb, initBomb.transform.position, initBomb.transform.rotation);

    }

    private void ataqueBombaLvl2()
    {
        GameObject bomb = Instantiate(Bomb, initBomb.transform.position, initBomb.transform.rotation);
        GameObject bomb2 = Instantiate(Bomb, initBomb.transform.position, initBomb.transform.rotation);
    }

    private void ataqueBombaLvl3()
    {
        GameObject bomb = Instantiate(Bomb, initBomb.transform.position, initBomb.transform.rotation);
        GameObject bomb2 = Instantiate(Bomb, initBomb.transform.position, initBomb.transform.rotation);
        GameObject bomb3 = Instantiate(Bomb, initBomb.transform.position, initBomb.transform.rotation);
    }
}
