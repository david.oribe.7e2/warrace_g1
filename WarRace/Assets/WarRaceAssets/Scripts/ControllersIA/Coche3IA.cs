﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coche3IA : Vehiculo
{

    public GameObject grapplingObj;
    public GameObject initGrap;

    public GameObject grapplingSprite;//grappling que siempre estará en el coche y que desactivaremos cuando ataque

    public float grapplingSpeed = 20f;
    public GameObject explosionDeadParticle;

    public GameObject saw1;
    public GameObject saw2;
    public GameObject saw3;
    public GameObject saw4;
    GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));
        lvlArmas = gameManager.shopC3[1];
    }

    // Update is called once per frame
    void Update()
    {
        if (lvlArmas == 1)
        {
            saw1.SetActive(true);
            saw2.SetActive(true);
            saw3.SetActive(false);
            saw4.SetActive(false);
        }
        else if (lvlArmas == 2)
        {
            saw1.SetActive(true);
            saw2.SetActive(true);
            saw3.SetActive(true);
            saw4.SetActive(false);
        }
        else
        {
            saw1.SetActive(true);
            saw2.SetActive(true);
            saw3.SetActive(true);
            saw4.SetActive(true);
        }
        if (raceing)
        {
            tiempoVuelta += Time.deltaTime;
        }
        if (attackActive)
        {
            grapplingSprite.SetActive(true);
        }
        else
        {
            grapplingSprite.SetActive(false);

        }
        if (life <= 0)
        {
            this.Dead();
        }
    }

    public override void Dead()
    {
        isDead = true;
        StartCoroutine(AnimDead());
        //Destroy(this.gameObject);
    }

    IEnumerator AnimDead()
    {
        GameObject explosionDead = Instantiate(explosionDeadParticle, this.transform.position, this.transform.rotation);
        yield return new WaitForSeconds(0.3f);
        this.gameObject.SetActive(false);

    }

    private void OnTriggerEnter(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && other.gameObject.tag != "triggerAttack")
        {
            if (attackActive)
            {
                grapplingSprite.SetActive(false);
                GameObject grappling = Instantiate(grapplingObj, initGrap.transform.position, initGrap.transform.rotation);
                Rigidbody rbGrap = grappling.GetComponent<Rigidbody>();
                rbGrap.velocity = initGrap.transform.TransformDirection(new Vector3(0, grapplingSpeed, 0));
                attackActive = false;
            }
            


        }
    }
}
