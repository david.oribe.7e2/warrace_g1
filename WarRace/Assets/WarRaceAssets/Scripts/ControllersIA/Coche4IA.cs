﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coche4IA : Vehiculo
{
    public Lanzallamas lanzallamas;

    [Header("Spikes Settings ------------------")]
    //public GameObject trampillaObj;
    public GameObject Spikes;

    public GameObject initSpikes;
    public Collider lanzallamasTrigger;

    private float timeToAttack1 = 0;

    public GameObject explosionDeadParticle;
    GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        lanzallamasTrigger.enabled = false;
        gameManager = (GameManager)FindObjectOfType(typeof(GameManager));

        lvlArmas = gameManager.shopC4[1];

    }

    // Update is called once per frame
    void Update()
    {
        if (raceing)
        {
            tiempoVuelta += Time.deltaTime;
        }
        if (attackActive)
        {
            timeToAttack1 += Time.deltaTime;
            if (timeToAttack1 > 5)
            {
                if (lvlArmas == 1)
                {
                    lanzarPinchos();
                }
                else if (lvlArmas == 2)
                {
                    lanzarPinchosLvl2();
                }
                else
                {
                    lanzarPinchosLvl3();
                }
                attackActive = false;
            }
        }
        else
        {
            lanzallamasTrigger.enabled = false;

            timeToAttack1 = 0;
        }
        if (life <= 0)
        {
            this.Dead();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null && other.gameObject.tag != "triggerAttack")
        {
            if (attackActive && timeToAttack1 < 5)
            {
                lanzallamasTrigger.enabled = true;

                //grapplingSprite.SetActive(false);
                StartCoroutine(lanzallamas.disparar());
                attackActive = false;


            }



        }
    }

    private void lanzarPinchos()
    {


        GameObject spikes = Instantiate(Spikes, initSpikes.transform.position, Spikes.transform.rotation);
        //canAttack2 = false;

        // }

    }

    private void lanzarPinchosLvl2()
    {


        GameObject spikes = Instantiate(Spikes, initSpikes.transform.position, Spikes.transform.rotation);
        GameObject spikes2 = Instantiate(Spikes, initSpikes.transform.position, Spikes.transform.rotation);

        // }

    }

    private void lanzarPinchosLvl3()
    {


        GameObject spikes = Instantiate(Spikes, initSpikes.transform.position, Spikes.transform.rotation);
        GameObject spikes2 = Instantiate(Spikes, initSpikes.transform.position, Spikes.transform.rotation);
        GameObject spikes3 = Instantiate(Spikes, initSpikes.transform.position, Spikes.transform.rotation);

        // }

    }

    public override void Dead()
    {
        isDead = true;
        StartCoroutine(AnimDead());
        //Destroy(this.gameObject);
    }

    IEnumerator AnimDead()
    {
        GameObject explosionDead = Instantiate(explosionDeadParticle, this.transform.position, this.transform.rotation);
        yield return new WaitForSeconds(0.3f);
        this.gameObject.SetActive(false);

    }
}
