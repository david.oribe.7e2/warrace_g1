﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WP_Coche : MonoBehaviour
{
    MetaController metaController;
    /*
     * Rigidbody _rb;
    Vehiculo vehiculo;
    [Header("Car Settings ------------------")]
    public Vector3 centerOfMass = Vector3.zero;
    public float motorForce;
    public float brakeForce;
    public float maxSteerAngle;
    public WheelCollider ruedaDDCollider;
    public WheelCollider ruedaDICollider;

    [Header("Attack Settings ---------------")]
    //public Image isAttackActive;
    //public Image attack1;
    //public Image attack2;
    public int attackSelected;
    private bool isAttackChanged;

    void FixedUpdate()
    {
        // dar fuerza a las ruedas para tirar hacia adelante
        motorForce = Input.GetAxis("Vertical") * vehiculo.cc;
        ruedaDDCollider.motorTorque = motorForce;
        ruedaDICollider.motorTorque = motorForce;

        // para que las ruedas giren el coche
        float rotation = Input.GetAxis("Horizontal") * maxSteerAngle;
        ruedaDDCollider.steerAngle = rotation;
        ruedaDICollider.steerAngle = rotation;
        //para que las ruedas giren gráficamente
        ruedaDDCollider.transform.localEulerAngles = new Vector3(0f, rotation, 0f);
        ruedaDICollider.transform.localEulerAngles = new Vector3(0f, rotation, 0f);

    }
    // Start is called before the first frame update
    void Start()
    {
        vehiculo = this.gameObject.GetComponent<Vehiculo>();

        _rb = this.gameObject.GetComponent<Rigidbody>();
        _rb.centerOfMass = centerOfMass;
        //-------------------------------------
        attackSelected = 1;
        isAttackChanged = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            isAttackChanged = !isAttackChanged;

            Debug.Log("changed Attack" + attackSelected);

        }

        if (isAttackChanged)
        {
            attackSelected = 2;
        }
        else
        {
            attackSelected = 1;
        }

        if (vehiculo.attackActive)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))//También havrá que poner como condición si el boolean attackActive estará activado. 
            {
                vehiculo.Attack();
                vehiculo.attackActive = false;
            }
        }
    }
     */
    // Start is called before the first frame update
    Rigidbody _rb;
    Vehiculo vehiculo;

    [Header("Car Settings ------------------")]
    public Vector3 centerOfMass = Vector3.zero;
    public float motorForce;
    public float brakeForce;
    public float maxSteerAngle;
    public WheelCollider ruedaDDCollider;
    public WheelCollider ruedaDICollider;
    public Transform target;

    public float timeToQuitGrap=0;
    void FixedUpdate()
    {
        // dar fuerza a las ruedas para tirar hacia adelante
        motorForce = vehiculo.cc;
        ruedaDDCollider.motorTorque = motorForce;
        ruedaDICollider.motorTorque = motorForce;

        brakeForce = vehiculo.frenada;
        ruedaDDCollider.brakeTorque = brakeForce;
        ruedaDDCollider.brakeTorque = brakeForce;

        // para que las ruedas giren el coche
        /*
        float rotation = Input.GetAxis("Horizontal") * maxSteerAngle;
        ruedaDDCollider.steerAngle = rotation;
        ruedaDICollider.steerAngle = rotation;
        //para que las ruedas giren gráficamente
        ruedaDDCollider.transform.localEulerAngles = new Vector3(0f, rotation, 0f);
        ruedaDICollider.transform.localEulerAngles = new Vector3(0f, rotation, 0f);
        */

    }
    void Start()
    {
        vehiculo = this.gameObject.GetComponent<Vehiculo>();
        vehiculo.isGrapped = false; 

        _rb = this.gameObject.GetComponent<Rigidbody>();
        _rb.centerOfMass = centerOfMass;
        //-------------------------------------
        //attackSelected = 1;
       // isAttackChanged = false;
        transform.LookAt(new Vector3(target.position.x, transform.position.y, target.position.z));
        metaController = (MetaController)FindObjectOfType(typeof(MetaController));

    }

    // Update is called once per frame
    void Update()
    {
        if (vehiculo.isGrapped)
        {
            timeToQuitGrap += Time.deltaTime;
            vehiculo.frenada = 10;
            transform.Translate(new Vector3(0, 0, -15 * Time.deltaTime));
            if (timeToQuitGrap > 3)
            {
                vehiculo.isGrapped = false;
            }
        }
        else if (!vehiculo.isGrapped && !metaController.startingRace && !metaController.finishedRace)
        {
            vehiculo.frenada = 0;
            transform.Translate(new Vector3(0, 0, 0));
        }

        transform.LookAt(new Vector3(target.position.x, target.position.y, target.position.z));

        //transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "WayPoint" && other.gameObject.tag != "triggerAttack")
        {
            target = other.gameObject.GetComponent<WayPoint>().nextPoint;
            transform.LookAt(new Vector3(target.position.x, target.position.y, target.position.z));
        }
    }
}
