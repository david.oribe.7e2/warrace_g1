﻿using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Database", menuName = "ListaTienda", order = 1)]
public class Database : ScriptableObject
{
    [System.Serializable]
    public struct Items
    {
        public string nombre;
        public int id;
        public int precio;
        public Tipo tipo;
        public Nivel nivel;
        public bool comprado;
    }

    public enum Tipo
    {
        Blindaje,
        CC,
        Armas
    }
    public enum Nivel
    {
        lvl1,
        lvl2,
        lvl3
    }

}
