﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadExplosion : MonoBehaviour
{
    private float timeToDespawn;

    void Start()
    {
  
    }

    // Update is called once per frame
    void Update()
    {
        timeToDespawn += Time.deltaTime;
        if (timeToDespawn > 3)
        {
            Destroy(this.gameObject);
        }
    }
}
