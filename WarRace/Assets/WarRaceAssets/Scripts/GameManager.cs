﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    MetaController metaController;
    static GameManager gameManager;
    [Header("Select Car Settings -------------------")]
    public bool car1 = false;
    public bool car2 = false;
    public bool car3 = false;
    public bool car4 = false;
    
    [Header("Player Settings ------------------------")]
    public int money;
    public int[] shopC1 = new int[3];
    public int[] shopC2 = new int[3];
    public int[] shopC3 = new int[3];
    public int[] shopC4 = new int[3];


    [Header("Selection Mode Settings ------------------------")]
    public ModeSelected modeSelected = ModeSelected.None;
    public enum ModeSelected
    {
        None,
        OneRace,
        GrandPrix,
        Versus
    }
    
    [Header("GrandPrix Settings ---------------------------")]
    public int racesDone = 0;
    public string nameCircuit1 = "CircuitoDesierto";
    public string nameCircuit2 = "CircuitoCiudad";
    public string nameCircuit3 = "CircuitoCarreras";
    private List<string> listaDeCircuitos = new List<string>();
    public string[] classificationList = new string[8];

    [Header("Achievements Settings ------------------------")]
    public int totalRacesDone = 0;
    public int racesSurvived = 0;
    public int totalMoneyEarned = 0;

    [Header("GameState Settings ------------------------")]
    public GameState gameState = GameState.Pause;
    public enum GameState
    {
        Playing,
        Pause
    }

    void Awake()
    {
        //hay que hacer el Singleton de DontDestroyOnLoad()
        if (gameManager == null)
        {
            gameManager = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }

    }
    private void iniciarValoresShop()
    {
        money = PlayerPrefs.GetInt("money", 500); //MONEDAS INICIALES
        shopC1[0] = PlayerPrefs.GetInt("armorC1", 100); //valor de vida real que se guardará del coche1
        shopC1[1] = PlayerPrefs.GetInt("weaponsC1", 1); //NIVEL DE LAS ARMAS que se guardará del coche1
        shopC1[2] = PlayerPrefs.GetInt("ccC1", 500); //valor de cc real que se guardará del coche1 HAY QUE MIRAR BIEN EL CC DE LOS COCHES Q FUNCIONE BIEN!!!
        shopC2[0] = PlayerPrefs.GetInt("armorC2", 100); //valor de vida real que se guardará del coche2
        shopC2[1] = PlayerPrefs.GetInt("weaponsC2", 1); //NIVEL DE LAS ARMAS que se guardará del coche2
        shopC2[2] = PlayerPrefs.GetInt("ccC2", 500); //valor de cc real que se guardará del coche2 HAY QUE MIRAR BIEN EL CC DE LOS COCHES Q FUNCIONE BIEN!!!
        shopC3[0] = PlayerPrefs.GetInt("armorC3", 100); //valor de vida real que se guardará del coche3
        shopC3[1] = PlayerPrefs.GetInt("weaponsC3", 1); //NIVEL DE LAS ARMAS que se guardará del coche3
        shopC3[2] = PlayerPrefs.GetInt("ccC3", 500); //valor de cc real que se guardará del coche3 HAY QUE MIRAR BIEN EL CC DE LOS COCHES Q FUNCIONE BIEN!!!
        shopC4[0] = PlayerPrefs.GetInt("armorC4", 100); //valor de vida real que se guardará del coche4
        shopC4[1] = PlayerPrefs.GetInt("weaponsC4", 1); //NIVEL DE LAS ARMAS que se guardará del coche4
        shopC4[2] = PlayerPrefs.GetInt("ccC4", 500); //valor de cc real que se guardará del coche4 HAY QUE MIRAR BIEN EL CC DE LOS COCHES Q FUNCIONE BIEN!!!

    }
    private void inicaiarValoresLogros()
    {
        totalRacesDone = PlayerPrefs.GetInt("totalRacesDone", 0);
        racesSurvived = PlayerPrefs.GetInt("racesSurvived", 0);
        totalMoneyEarned = PlayerPrefs.GetInt("totalMoneyEarned", 0);
    }
    // Start is called before the first frame update
    void Start()
    {
        
        listaDeCircuitos.Add(nameCircuit1);
        listaDeCircuitos.Add(nameCircuit3);
        listaDeCircuitos.Add(nameCircuit2);
        PlayerPrefs.DeleteAll(); //COMENTAR PARA QUE SE GUARDEN LOS DATOS!!!!

        iniciarValoresShop();
        inicaiarValoresLogros();

        //------------------------------------------

        //... TODO LOS DEMÁS

        // cagar los datos de partida
    }
    /*
     * método al que llamará MetaController para decirle que ha terminado la carrera
     * este mirará en que modo de juego está
     * si es una carrera termina, si es grandPrix y no es el último circuito continua
     */
    public void raceEnded() 
    {
        PlayerPrefs.SetInt("racesSurvived", PlayerPrefs.GetInt("racesSurvived") + 1);
        PlayerPrefs.SetInt("totalRacesDone", PlayerPrefs.GetInt("totalRacesDone") + 1);

        if (modeSelected == ModeSelected.OneRace)
        {
            modeSelected = ModeSelected.None;
            gameState = GameState.Pause;
            desactivateCars();
            PlayerPrefs.SetInt("money", money);
            SceneManager.LoadScene("Inicio", LoadSceneMode.Single);
        }
        else if (modeSelected == ModeSelected.GrandPrix)
        {
            racesDone++;
            if (racesDone >= listaDeCircuitos.Count)
            {
                // ordenar classificationList según los puntos de los coches
                modeSelected = ModeSelected.None;
                gameState = GameState.Pause;
                racesDone = 0;
                restartPuntuation();
                desactivateCars();
                PlayerPrefs.SetInt("money", money);

                SceneManager.LoadScene("Inicio", LoadSceneMode.Single);

            }
            else
            {
                // ordenar classificationList según los puntos de los coches
                //desactivateCars();
                PlayerPrefs.SetInt("money", money);

                SceneManager.LoadScene(listaDeCircuitos[racesDone], LoadSceneMode.Single); // cambia a el siguiente circuito

            }
        }
        else if (modeSelected == ModeSelected.Versus)
        {
            modeSelected = ModeSelected.None;
            gameState = GameState.Pause;
            desactivateCars();
            PlayerPrefs.SetInt("money", money);

            SceneManager.LoadScene("Inicio", LoadSceneMode.Single);
        }
    }

    public void youDead()
    {
        PlayerPrefs.SetInt("totalRacesDone", PlayerPrefs.GetInt("totalRacesDone") + 1);
        if (modeSelected == ModeSelected.OneRace)
        {
            modeSelected = ModeSelected.None;
            gameState = GameState.Pause;
            desactivateCars();
            SceneManager.LoadScene("Inicio", LoadSceneMode.Single);
        }
        else if (modeSelected == ModeSelected.GrandPrix)
        {
           
                // ordenar classificationList según los puntos de los coches
                modeSelected = ModeSelected.None;
                gameState = GameState.Pause;
                racesDone = 0;
                restartPuntuation();
            desactivateCars();
            SceneManager.LoadScene("Inicio", LoadSceneMode.Single);
        }
        else if (modeSelected == ModeSelected.Versus)
        {
            modeSelected = ModeSelected.None;
            gameState = GameState.Pause;
            desactivateCars();
            SceneManager.LoadScene("Inicio", LoadSceneMode.Single);
        }
    }


    private void restartPuntuation()
    {
        metaController = (MetaController)FindObjectOfType(typeof(MetaController));
        for (int i = 0; i < metaController.allCars.Length; i++)
        {
            Vehiculo coche = metaController.allCars[i].GetComponent<Vehiculo>();
            coche.puntos = 0;
        }
    }

    private void desactivateCars()
    {
        car1 = false;
        car2 = false;
        car3 = false;
        car4 = false;
    }



}
