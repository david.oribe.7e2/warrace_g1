﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Hover : MonoBehaviour
{
    //Varaibles publcias
    public float DistanciaVuela;
    public float Acceleracion;
    public float VelocidadMaxima;
    public float VelocidadRotacion;

    //Variables privadas
    Vector3 PushVector;

    //Referencias
    Rigidbody rigi;
    ConstantForce constForce;

	//Solo para control
	[System.NonSerialized]
	public float Vertical;
	[System.NonSerialized]
	public float Horizontal;

    private Vector3 imp = new Vector3(25,0,25);
    private bool isOnCollision = false;
    Vehiculo vehiculo;

    void Start ()
    {
        rigi = GetComponent<Rigidbody>();
        constForce = GetComponent<ConstantForce>();
        vehiculo = this.gameObject.GetComponent<Vehiculo>();

    }

    private void Update()
    {

       
        
        PushVector = transform.forward * Vertical; //Mueve hacia delante

        

        PushVector.y = 0f;


        if(Horizontal != 0f)
        {
            transform.Rotate(0f, VelocidadRotacion * Horizontal * Time.deltaTime, 0f);
        }
    }	
	
	void FixedUpdate ()
    {
        RaycastHit hit;
        Ray rayo = new Ray(transform.position, Vector3.down);
        
        if(Physics.Raycast(rayo, out hit, DistanciaVuela))
        {
            constForce.enabled = true;
        }
        else
        {
            constForce.enabled = false;
        }

        if(VelocityWithoutY() < VelocidadMaxima) //Si no ha llegado a velocidad maxima
        {

            rigi.AddForce(PushVector * Acceleracion);
        }
	}

    float VelocityWithoutY()
    {
        Vector2 vel = new Vector2(rigi.velocity.x, rigi.velocity.z);
        return vel.magnitude;
    }
    void OnCollisionStay(Collision collision)
    {
        Vehiculo vehiculo = collision.gameObject.GetComponent<Vehiculo>();
        if (vehiculo != null||collision.gameObject.tag=="turret")
        {
            rigi.AddForce(imp*90, ForceMode.Impulse);
            //PushVector = -PushVector;
        }


    }
}
