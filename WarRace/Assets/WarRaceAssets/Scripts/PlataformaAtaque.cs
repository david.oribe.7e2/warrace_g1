﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaAtaque : MonoBehaviour
{
    public bool canGiveAttack;
    public float timeActivePlataform;
    private float startTimeActivePlataform;

    Renderer renderer;
    public Material green;
    public Material red;
    public AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
        canGiveAttack = true;
        startTimeActivePlataform = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!canGiveAttack)
        {
            startTimeActivePlataform += Time.deltaTime;
        }
        activePlataform();
        switchPlatformColor();

    }

    private void OnTriggerEnter(Collider other)
    {
        Vehiculo vehiculo = other.GetComponent<Vehiculo>();
        if (vehiculo != null&&other.gameObject.tag!="triggerAttack")
        {
            if (canGiveAttack)
            {
                audio.Play();
                vehiculo.attackActive = true;
                canGiveAttack = false;
            }

        }
    }
    private void activePlataform()
    {
        if (timeActivePlataform < startTimeActivePlataform)
        {
            canGiveAttack = true;
            startTimeActivePlataform = 0f;
        }
    }
    private void switchPlatformColor()
    {
        if (canGiveAttack)
        {
            renderer.material = green;
        }
        else
        {
            renderer.material = red;
        }
    }
}
